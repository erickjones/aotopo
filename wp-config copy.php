<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wordpress');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'darrel');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l7;%9R>!sT~q%P5vJnhr,[_{+!JC+nAUlp(<FE}1yq:;M`BH6SSjwiv8vs[3SLQx');
define('SECURE_AUTH_KEY',  ';1=gIbP74pii`=A/N[5Z<|8~ZwEtXNZ7DcqkkAyOv;r=&iHf<+ck@tvP1.TjUFe9');
define('LOGGED_IN_KEY',    '^rGo ACZ1^^IrAn%q9x|mn+v9Oct,yKotdN F$Ra,:We.+o?yw^9+$*f)mZy)12m');
define('NONCE_KEY',        '@CW:,pF|m,GQ=Aj9Gl`v?cO]9{w:^&=>c4r-3a-Oz;SN3d%Z-yv!!D9.R_X^L^k[');
define('AUTH_SALT',        '$goIfFU2g<e;H-=$`Hx0owupY-s-lM7{eP:Xo#7m@DF^#S8) t&Kas+f7wxhCsK-');
define('SECURE_AUTH_SALT', '6e]h+(HxUh}pU]i<3Z=~!aMq&aiVRR}X;(!%j7*cXq*n]5dG13;NpcK/+s,9 4!l');
define('LOGGED_IN_SALT',   '4j F.C75$huf7F3Y}:)<cG$[k*g+QiFxq{Ni%2@^&I<A>X#8xuO+1/>fWP}LvwOx');
define('NONCE_SALT',       '>8VCJ+|g1&=B=~)EflDP5*N6lr~+^dO/iTQ8RQ~/iMxT|+oCl,Y[.0h2wTM~67o$');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_aotopo_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
