=== Google Plus Badge Widget ===
Contributors: Paulund
Tags: Wordpress, Google, plus, badge
Requires at least: 3.0
Tested up to: 3.3
Stable tag: trunk
 
Adds a Google Plus widget to your Wordpress blog.

== Description ==

This Wordpress plugin will add a Google Plus widget to your Wordpress blog.

This will allow you to choose between icon, small badge, large badge.

You can choose the width of the badge.

This plugin will also allow you to choose with the Google profile is the publisher or the author of the content.


== Installation ==

1. Upload `paulund-google-badge-widget.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

== Upgrade Notice ==

== Arbitrary section ==
