=== Author Bio Box ===
Contributors: claudiosanches, gustavo-freitas
Tags: Social, author, bio, social
Requires at least: 3.2
Tested up to: 3.4.1
Stable tag: 1.7.1

Exiba o perfil do autor do post em baixo de todas os posts de seu blog

== Description ==

O Author Bio Box exibe informa&ccedil;&otilde;es do autor em baixo de cada post do blog de forma autom&aacute;tica.
Utilize as inforam&ccedil;&otilde;es encontradas em seu perfil para mostrar NOME, REDES SOCIAIS e BIOGRAFIA.

== Installation ==

Instale Author Bio Box atrav&eacute;s do diret&oacute;rio de plugin WordPress.org, ou enviando os arquivos para o servidor e inserindo na pasta wp-content/plugins.
Depois de instalado basta logar em seu blog e ir em Configura&ccedil;&otilde;es > Author Bio Box, para configurar suas op&ccedil;&otilde;es.
Use as informa&ccedil;&otilde;es do perfil do usu&aacute;rio como Nome de "Exibir o nome publicamente como", "Informa&ccedil;&otilde;es biogr&aacute;ficas" e etc para serem exibidas pelo plugin em baixo dos seus posts.

== License ==

This file is part of Author Bio Box.
Author Bio Box is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Author Bio Box is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Author Bio Box. If not, see <http://www.gnu.org/licenses/>.

== Frequently Asked Questions ==

= Can I suggest a feature for the plugin? =
Of course, visit [Author Bio Box](http://www.claudiosmweb.com/)

== Changelog ==

= 1.7.1 =
* Fixed jQuery
* Fixed general styles
* Fixed wp_nonce

= 1.7 =
* Corrigido o bug da opção de desativar a inserção automática do plugin

= 1.6 =automatica
* Melhoria na performance
* Adicionada opção para inserir manualmente o plugin no tema

= 1.5 =
* Melhoria na seguran&ccedil;a do plugin evitando inser&ccedil;&atilde;o de dados maliciosos atrav&eacute;s dos campos do menu

= 1.4 =
* Melhoria na segurança do plugin usando wp_nonce_field e check_admin_referer

= 1.3 =
* Adicionada op&ccedil;&atilde;o para exibir a biografia tamb&eacute;m na p&aacute;gina inicial.

= 1.2 =
* Inserido target=&quot;_blank&quot; em todos os links da descri&ccedil;&atilde;o do autor.

= 1.1 =
* Corre&ccedil;&atilde;o do script, remo&ccedil;&atilde;o de tags obsoletas do wordpress e inser&ccedil;&atilde;o de target=&quot;_blank&quot; nos &iacute;cones sociais.

= 1.0 =
* Cria&ccedil;&atilde;o de desenvolvimento do plugin

== Screenshots ==
1. Plugin em funcionamento.
2. Op&ccedil;&otilde;es do plugin