<?php
/*
Plugin Name: Author Bio Box
Plugin URI: http://www.ferramentasblog.com/2011/09/power-comment-validacao-de-comentarios.html
Description: Exiba um box com a biografia do autor dos posts e também links de redes sociais.
Author: Claudio Sanches
Version: 1.7.1
Author URI: http://www.claudiosmweb.com/
*/

// Criar menu para o plugin no WP
function add_authorbbox_menu() {
    add_options_page('Author Bio Box', 'Author Bio Box', 'manage_options', __FILE__, 'admin_authorbbox');
}
add_action('admin_menu', 'add_authorbbox_menu');
// Adicionar opcoes no DB
function set_authorbbox_options() {
    add_option('authorbbox_img','70');
    add_option('authorbbox_bg','#f8f8f8');
    add_option('authorbbox_bwidth','2');
    add_option('authorbbox_bstyle','Solida');
    add_option('authorbbox_bcolor','#cccccc');
    add_option('authorbbox_show','posts');
    add_option('authorbbox_function','1');
}
// Deleta opcoes quando o plugin &eacute; desinstalado
function unset_authorbbox_options() {
    delete_option('authorbbox_img');
    delete_option('authorbbox_bg');
    delete_option('authorbbox_bwidth');
    delete_option('authorbbox_bstyle');
    delete_option('authorbbox_bcolor');
    delete_option('authorbbox_show');
    delete_option('authorbbox_function');
}
// instrucoes ao instalar ou desistalar o plugin
register_activation_hook(__FILE__,'set_authorbbox_options');
register_deactivation_hook(__FILE__,'unset_authorbbox_options');

// Pagina de opcoes
function admin_authorbbox() {
    ?>
    <div class="wrap">
        <div class="icon32" id="icon-options-general"><br /></div>
        <h2>Author Bio Box Op&ccedil;&otilde;es</h2>
        <?php
        if(!empty($_POST) && check_admin_referer(plugin_basename(__FILE__), 'authorbbox_nonce_field')) {
            update_authorbbox_options();
        }
        print_authorbbox_form();
        ?>
    </div>
    <?php
}
// Validar op&ccedil;&otilde;es
function update_authorbbox_options() {
    $correto = false;
    // Tamanho da imagem
    if ($_REQUEST['authorbbox_img']) {
        update_option('authorbbox_img', $_REQUEST['authorbbox_img']);
        $correto = true;
    }
    // Cor de fundo
    if ($_REQUEST['authorbbox_bg']) {
        update_option('authorbbox_bg', $_REQUEST['authorbbox_bg']);
        $correto = true;
    }
    // Largura da borda
    if ($_REQUEST['authorbbox_bwidth']) {
        update_option('authorbbox_bwidth', $_REQUEST['authorbbox_bwidth']);
        $correto = true;
    }
    // Estilo da borda
    if ($_REQUEST['authorbbox_bstyle']) {
        update_option('authorbbox_bstyle', $_REQUEST['authorbbox_bstyle']);
        $correto = true;
    }
    // Cor da borda
    if ($_REQUEST['authorbbox_bcolor']) {
        update_option('authorbbox_bcolor', $_REQUEST['authorbbox_bcolor']);
        $correto = true;
    }
    // Onde mostrar
    if ($_REQUEST['authorbbox_show']) {
        update_option('authorbbox_show', $_REQUEST['authorbbox_show']);
        $correto = true;
    }
    // Desativar codigo automatico
    if ($_REQUEST['authorbbox_function']) {
        update_option('authorbbox_function', $_REQUEST['authorbbox_function']);
        $correto = true;
    }
    if ($correto) {
        ?><div id="message" class="updated fade">
        <p><?php _e('Op&ccedil;&otilde;es salvas.'); ?></p>
        </div> <?php
    }
    else {
        ?><div id="message" class="error fade">
        <p><?php _e('Erro ao salvar op&ccedil;&otilde;es!'); ?></p>
        </div><?php
    }
}

// Formulario com as opcoes
function print_authorbbox_form() {
    $default_img = get_option('authorbbox_img');
    $default_bg = get_option('authorbbox_bg');
    $default_bwidth = get_option('authorbbox_bwidth');
    $default_bstyle = get_option('authorbbox_bstyle');
    $default_bstyle_options = array('Sem borda','Solida','Pontilhada','Tracejada');
    $default_bcolor = get_option('authorbbox_bcolor');
    $default_show = get_option('authorbbox_show');
    $default_function = get_option('authorbbox_function');
    $authorbbox_plugin_dir = get_bloginfo('wpurl') . '/wp-content/plugins/author-bio-box/';
    ?>
    <form action="" method="post">
    <h3 style="margin: 20px 0 -5px;"><?php _e('Apar&ecirc;ncia'); ?></h3>
    <table class="form-table">
        <tr>
            <th scope="row"><label for="authorbbox_img_op"><?php _e('Tamanho da imagem do autor'); ?></label></th>
            <td>
                <input type="text" class="regular-text" name="authorbbox_img" id="authorbbox_img_op" value="<?php echo strip_tags($default_img); ?>" />
                <br /><span class="description"><?php _e('Tamanho do gravatar do autor do blog. O autor precisa ter seu e-mail cadastrado com imagem no <a href="http://pt.gravatar.com/">Gravatar.com</a> (Aprenda como configurar seu gravatar <a href="http://gfsolucoes.net/gravatar-o-que-e-como-ter-um-como-colocar-em-seu-blog/">aqui</a>.)<br />(Insira n&uacute;meros inteiros).'); ?></span>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="color_abb_bg_op"><?php _e('Cor de fundo'); ?></label></th>
            <td>
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('#ilctabscolorpicker_abb_bg').hide();
                        $('#ilctabscolorpicker_abb_bg').farbtastic("#color_abb_bg_op");
                        $("#color_abb_bg_op").click(function(){
                            $('#ilctabscolorpicker_abb_bg').slideToggle();
                        });
                    });
                </script>
                <input style="width:60px;" type="text" class="regular-text" name="authorbbox_bg" id="color_abb_bg_op" value="<?php echo strip_tags($default_bg); ?>" />
                <div id="ilctabscolorpicker_abb_bg"></div>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="authorbbox_bwidth_op"><?php _e('Largura da borda'); ?></label></th>
            <td>
                <input type="text" class="regular-text" name="authorbbox_bwidth" id="authorbbox_bwidth_op" value="<?php echo strip_tags($default_bwidth); ?>" />
                <br /><span class="description"><?php _e('Espessura da borda superior e inferior do box<br />(Insira n&uacute;meros inteiros).'); ?></span>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="authorbbox_bstyle_op"><?php _e('Estilo da borda'); ?></label></th>
            <td>
                <select style="width:120px;" name="authorbbox_bstyle" id="authorbbox_bstyle_op">
                <?php foreach ($default_bstyle_options as $option) { ?>
                    <option <?php if ($default_bstyle == $option) { echo 'selected="selected"'; } ?>><?php echo strip_tags($option); ?></option><?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="color_abb_border_op"><?php _e('Cor da borda'); ?></label></th>
            <td>
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('#ilctabscolorpicker_abb_border').hide();
                        $('#ilctabscolorpicker_abb_border').farbtastic("#color_abb_border_op");
                        $("#color_abb_border_op").click(function(){
                            $('#ilctabscolorpicker_abb_border').slideToggle();
                        });
                    });
                </script>
                <input style="width:60px;" type="text" class="regular-text" name="authorbbox_bcolor" id="color_abb_border_op" value="<?php echo strip_tags($default_bcolor); ?>" />
                <div id="ilctabscolorpicker_abb_border"></div>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="authorbbox_show_op1"><?php _e('Mostrar plugin em'); ?></label></th>
            <td>
                <label><input type="radio" id="authorbbox_show_op1" name="authorbbox_show" value="posts" <?php if ($default_show == 'posts') { _e('checked="checked"'); } ?> /> <?php _e('Apenas dentro dos Posts'); ?></label>
                <label><input style="margin:0 0 0 10px" type="radio" id="authorbbox_show_op2" name="authorbbox_show" value="home" <?php if ($default_show == 'home') { _e('checked="checked"'); } ?>/> <?php _e('Na p&aacute;gina inicial e posts'); ?></label>
            </td>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="authorbbox_function_op1"><?php _e('Inserir automáticamente o plugin no tema'); ?></label></th>
            <td>
                <label><input type="radio" id="authorbbox_function_op1" name="authorbbox_function" value="1" <?php if ($default_function == '1') { _e('checked="checked"'); } ?> /> <?php _e('Sim'); ?></label>
                <label><input style="margin:0 0 0 10px" type="radio" id="authorbbox_function_op2" name="authorbbox_function" value="2" <?php if ($default_function == '2') { _e('checked="checked"'); } ?>/> <?php _e('Não'); ?></label>
                <br /><span class="description"><?php _e('Para inserir manualmente utilize esta função: <code>&lt;?php if ( function_exists( \'authorbbio_add_authorbox\' ) ) authorbbio_add_authorbox(); ?&gt;</code>'); ?></span>
            </td>
            </td>
        </tr>
    </table>
    <p class="submit">
        <?php wp_nonce_field(plugin_basename(__FILE__), 'authorbbox_nonce_field'); ?>
        <input type="submit" class="button-primary" name="submit" value="salvar" />
    </p>
    <p>
        <a style="margin-right:20px;" href="http://www.fbloghost.com/plano-wp-host/" target="_blank" title="FBlogHost - Hospedagem profissional para Worpdress">
            <img style="border:none;" src="<?php echo $authorbbox_plugin_dir; ?>/fbloghost.jpg" alt="FBlogHost - Hospedagem profissional para Worpdress" />
        </a>
        <a href="http://gustavofreitas.net/ebookfeedburner/" target="_blank" title="Como transformar seu Blogspot em um Blog Profissional">
            <img style="border:none;" src="<?php echo $authorbbox_plugin_dir; ?>/feed-e-feedburner.gif" alt="Feed e FeedBurner - Aprenda tudo que você precisa para ganhar dinheiro com eles" />
        </a>
    </p>
    </form>
<?php
}
// Chama Color Picker
function authorbbox_color() {
    wp_enqueue_style('farbtastic');
    wp_enqueue_script('farbtastic');
}
add_action('admin_menu', 'authorbbox_color');
// JS e CSS do plugin no head
function authorbbox_css_head() {
    $uthorbbox_css_bstyle = get_option('authorbbox_bstyle');
    switch($uthorbbox_css_bstyle) {
        case 'Sem borda':
            $uthorbbox_css_bstyle = 'none';
            break;
        case 'Solida':
            $uthorbbox_css_bstyle = 'solid';
            break;
        case 'Pontilhada':
            $uthorbbox_css_bstyle = 'dotted';
            break;
        case 'Tracejada':
            $uthorbbox_css_bstyle = 'dashed';
            break;
    }
    $author_show = get_option('authorbbox_show');
    switch($author_show) {
        case 'posts':
            $author_show_in = is_single();
            break;
        case 'home':
            $author_show_in = is_single() || is_home() || is_front_page();
            break;
    }
    if($author_show_in) {
        echo '<style type="text/css">
    #blog-autor * {margin:0;padding:0;}
    #blog-autor {border-width:'.get_option('authorbbox_bwidth').'px 0 '.get_option('authorbbox_bwidth').'px;border-style:'.$uthorbbox_css_bstyle.';border-color:'.get_option('authorbbox_bcolor').';background:'.get_option('authorbbox_bg').';padding:10px 10px 0;margin:10px 0;}
    #blog-autor h3,p#autor-desc {margin:0 0 10px;}
    #blog-autor a img {background:none;margin:0 3px 0 0;border:none;opacity:1;transition:all 0.4s ease;-webkit-transition:all 0.4s ease;-o-transition:all 0.4s ease;-moz-transition:all 0.4s ease;}
    #blog-autor a:hover img {background:none;border:none;opacity:0.7;}
    #autor-gravatar {float:left;}
    #autor-gravatar img {margin:0 15px 0 0;background:#fff;border:1px solid #ccc;padding:3px;}
    p#autor-social {margin:0 0 5px;}
    .clear {clear:both;}
</style>
';
    }
}
add_filter('wp_head', 'authorbbox_css_head');
// Modifica o perfil padr&atilde;o do Wordpress
function authorbbio_contact_edt($authorbbio_contact) {
    $authorbbio_contact['facebook'] = 'Facebook';
    $authorbbio_contact['twitter'] = 'Twitter';
    $authorbbio_contact['googleplus'] = 'Google Plus';
    $authorbbio_contact['linkedin'] = 'LinkedIn';
    unset($authorbbio_contact['aim']);
    unset($authorbbio_contact['yim']);
    unset($authorbbio_contact['jabber']);
    return $authorbbio_contact;
}
add_filter('user_contactmethods','authorbbio_contact_edt',10,1);
// Mostra facebook se existir link
function authorbbio_add_facebook() {
    $authorbbio_facebook = get_the_author_meta('facebook');
    if($authorbbio_facebook == '' || $authorbbio_facebook == null) {
        return null;
    }
    else {
        return '<a target="_blank" href="'. $authorbbio_facebook .'"><img src="'. get_bloginfo('wpurl') .'/wp-content/plugins/author-bio-box/facebook.png" alt="facebook" /></a>';
    }
}
// Mostra twitter se existir link
function authorbbio_add_twitter() {
    $authorbbio_twitter = get_the_author_meta('twitter');
    if($authorbbio_twitter == '' || $authorbbio_twitter == null) {
        return null;
    }
    else {
        return '<a target="_blank" href="'. $authorbbio_twitter .'"><img src="'. get_bloginfo('wpurl') .'/wp-content/plugins/author-bio-box/twitter.png" alt="twitter" /></a>';
    }
}
// Mostra google plus se existir link
function authorbbio_add_googleplus() {
    $authorbbio_googleplus = get_the_author_meta('googleplus');
    if($authorbbio_googleplus == '' || $authorbbio_googleplus == null) {
        return null;
    }
    else {
        return '<a target="_blank" href="'. $authorbbio_googleplus .'"><img src="'. get_bloginfo('wpurl') .'/wp-content/plugins/author-bio-box/google-plus.png" alt="google plus" /></a>';
    }
}
// Mostra linkedin se existir link
function authorbbio_add_linkedin() {
    $authorbbio_linkedin = get_the_author_meta('linkedin');
    if($authorbbio_linkedin == "" || $authorbbio_linkedin == null) {
        return null;
    }
    else {
        return '<a target="_blank" href="'. $authorbbio_linkedin .'"><img src="'. get_bloginfo('wpurl') .'/wp-content/plugins/author-bio-box/linkedin.png" alt="linkedin" /></a>';
    }
}
// Adiciona Author Bio Box
function authorbbio_add_box($content) {
    $authorbbio_content = '
<div id="blog-autor">
    <div id="autor-gravatar">'. get_avatar(get_the_author_meta('ID'), get_option('authorbbox_img')) .'</div>
    <div id="autor-bio">
        <h3>'. get_the_author_meta('display_name') .'</h3>
        
        <p id="autor-social">'. authorbbio_add_facebook() . authorbbio_add_twitter() . authorbbio_add_googleplus() . authorbbio_add_linkedin() .'</p>
        <p id="autor-desc">'. str_replace('<a href=', '<a target="_blank" href=', get_the_author_meta('description')) .'</p>
        <p id="autor-footer"><a href="'. get_bloginfo('url') .'/?author='. get_the_author_meta('ID') .'">'. get_the_author_meta('display_name') .'</a> j&aacute; escreveu '. get_the_author_posts() .' posts.</p>
        <br class="clear" />
    </div>
</div>
';
    $author_show = get_option('authorbbox_show');
    switch($author_show) {
        case 'posts':
            $author_show_in = is_single();
            break;
        case 'home':
            $author_show_in = is_single() || is_home() || is_front_page();
            break;
    }
    if($author_show_in) {
        return $content . $authorbbio_content;
    }
    elseif(is_page()) {
        return $content;
    }
    else {
        return $content;
    }
}
if(get_option('authorbbox_function') == '1'){
    add_filter('the_content', 'authorbbio_add_box', 1300);
}
// Adiciona Author Bio Box manualmente
function authorbbio_add_authorbox() {
    echo '
<div id="blog-autor">
    <div id="autor-gravatar">'. get_avatar(get_the_author_meta('ID'), get_option('authorbbox_img')) .'</div>
    <div id="autor-bio">
        <h3>'. get_the_author_meta('display_name') .'</h3>
        <p id="autor-social">'. authorbbio_add_facebook() . authorbbio_add_twitter() . authorbbio_add_googleplus() . authorbbio_add_linkedin() .'</p>
        <p id="autor-desc">'. str_replace('<a href=', '<a target="_blank" href=', get_the_author_meta('description')) .'</p>
        <p id="autor-footer"><a href="'. get_bloginfo('url') .'/?author='. get_the_author_meta('ID') .'">'. get_the_author_meta('display_name') .'</a> j&aacute; escreveu '. get_the_author_posts() .' posts.</p>
        <br class="clear" />
    </div>
</div>
';
}
?>