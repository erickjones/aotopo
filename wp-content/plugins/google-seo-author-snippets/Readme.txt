=== Google Seo Author Snippet Plugin === 
Contributors: Smackcoders 
Donate link: http://www.smackcoders.com/donate.html
Tags: google, plugin, admin, comments, twitter, links, author, custom, dashboard, facebook,  
Requires at least: 3.4 
Tested up to: 3.4.2 
Stable tag: 1.0.1 
Author: Smackcoders
Author URI: http://profiles.wordpress.org/smackcoders/
License: GPLv2 or later

A powerful plugin to include microdata with extended authors social profiles built specially for seo and social perspective. 


== Description == 

Google Seo Author Snippet Plugin gives you both seo and social advantage.  

1. Allow authors to display their social profile links next to "posted by author" detail.  
2. Includes google+ profile with recomended rel=author tag 
3. Includes facebook, twitter and linkedin profiles in microdata (schema / snippet view / google structure data) 
4. And also can add their geo-location details in microdata. 
5. Can disable all ot one of these by leaving it as empty. 
6. Also both registered users and authors can display their profiles in comments.  


SEO And Social Advantages 
------------------------- 
-	Seo benefits to increase your rank by default. 
-	Displays your content in search results as google structured data, increase CTR.  
-	Real social advantage makes users to go social. 
-	Encourage users to register even to comment a post. 
-	Keeps social lovers stay in your site as to get build their social relationships. 
-	Serves as a great resource to find and build social networking. 
-	In over all reduces the bounce rate. 
-	Turns every visitor into unique and frequent user. 


Planned Feature and Recommended 
-------------------------------- 

Have a list of authors and users with their social profiles in a page or post. This will serve like a Social Profile cum user directory. 
You can use a existing plugin for now. As we have planned to add this in future release. And that will be within couple of weeks. Also encourage your visitors to register to get listed here. Or lock this content for registered users only. Don�t forget to keep a catchy social share buttons on top and bottom of content to get viral. By this you can gain more free traffic.  


Support and Feature requests. 
---------------------------- 
Please visit http://www.smackcoders.com/category/free-wordpress-plugins/google-seo-author-snippet-plugin.html for guides and tutorials. 
For quick response and reply please create issues in our [support](http://code.smackcoders.com/seo-author-snippet-wp-plugin/issues) instead of wordpress support forum. Support at wordpress is not possible for now. 


== Installation == 

Google Seo Author Snippet Plugin is very easy to install like any other wordpress plugin. No need to edit or modify anything here. 

1.    Unzip the file 'google_seo_autho_snippet_plugin.zip'. 
2.    Upload the ' google_seo_autho_snippet_plugin ' directory to '/wp-content/plugins/' directory using ftp client or upload and install google_seo_autho_snippet_plugin.zip through plugin install wizard in wp admin panel. 
3.    Activate the plugin through the 'Plugins' menu in WordPress. 
4.    After activating, you will see an option for 'Google Seo Author Snippet' in the admin menu (left navigation) and you will see an option the Microdata Social profile information and User Geo-Information in user profile settings page. 


== Screenshots == 

1. Admin settings for Google Seo Author Snippet Plugin . 
2. Admin settings for Image set configuration to the front-end. 
3. Microdata Social profile information and User Geo-Information for user profile settings. 
4. Preview form placed in widget area to the front-end. 


== Changelog == 

=1.0.1=		Image Conflicts Bug Fixed, Added Extra Social Icon sets.

=1.0.0=		Initial release version. Tested and found works well without any issues. 




== Upgrade Notice == 
=v 1.0.1=		Bug fix - Image conflicts, Added Extra Social Icon sets.


=v 1.0.0=		Initial release of plugin 


== Frequently Asked Questions == 

1. How to install the plugin? 
   Like other plugins google_seo_autho_snippet_plugin is easy to install. Upload the google_seo_autho_snippet_plugin.zip file through plugin install page through wp admin. Everything will work fine with it. 

2. How to configure the plugin? 

  After plugin activation you can see the ' Google Seo Author Snippet Plugin ' menu in admin backend. 

1. By default plugin is enabled on activation 
2. You can disable on rare cases like for maintenance / testing 
3. Choose your style of icon set 
4. Select which social profile to be included 
5. Save settings  

Configuring our plugin is as simple as that. 

3. How the users configure their social profile? 
   To enable users need to fill their social profile id or leave it empty to disable. 
   Users can use the help note and reference link under each field. 
    
For quick response and reply please create issues in our [support](http://code.smackcoders.com/seo-author-snippet-wp-plugin/issues) instead of wordpress support forum.


