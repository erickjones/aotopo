<?php
// Template Name: Contact
get_header(); ?>
<?php
//If the form is submitted
if(isset($_POST['submit'])) {
	//Check to make sure that the name field is not empty
	if(trim($_POST['contact_name']) == '' || trim($_POST['contact_name']) == 'Name (required)') {
		$hasError = true;
	} else {
		$name = trim($_POST['contact_name']);
	}

	//Check to make sure that the subject field is not empty
	if(trim($_POST['url']) == '' || trim($_POST['url']) == 'Subject (required)') {
		$hasError = true;
	} else {
		$subject = trim($_POST['url']);
	}

	//Check to make sure sure that a valid email address is submitted
	if(trim($_POST['email']) == '' || trim($_POST['email']) == 'Email (required)')  {
		$hasError = true;
	} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
	}

	//Check to make sure comments were entered
	if(trim($_POST['comment']) == '' || trim($_POST['comment']) == 'Message ...') {
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['comment']));
		} else {
			$comments = trim($_POST['comment']);
		}
	}

	//If there is no error, send the email
	if(!isset($hasError)) {
		$emailTo = get_post_meta($post->ID, 'pyre_email', true); //Put your own email address here
		$body = "Name: $name \n\nEmail: $email \n\nSubject: $subject \n\nComments:\n $comments";
		$headers = 'From: '.get_bloginfo('name').' <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

		mail($emailTo, $subject, $body, $headers);
		
		$emailSent = true;
	}
}
?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">
							<?php while(have_posts()): the_post(); ?>
							<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">
								<div class="heading"></div>
								<div class="post" style="padding: 0;">
									<?php if(get_post_meta($post->ID, 'pyre_gmap', true)): ?>
									<script type="text/javascript">
									jQuery(document).ready(function($) {
										$('.featured-image small, br').remove();
									});
									</script>
									<div class="featured-image">
										<?php echo get_post_meta($post->ID, 'pyre_gmap', true); ?>
									</div>
									<?php endif; ?>
									<h3><?php the_title(); ?></h3>
									<?php if(get_post_meta($post->ID, 'pyre_subtitle', true)): ?>
									<div class="meta"><?php echo get_post_meta($post->ID, 'pyre_subtitle', true); ?></div>
									<?php endif; ?>
									<div class="post-content portfolio-content">
										<div class="actual-portfolio-content">
											<?php the_content(); ?>
										</div>
										<div class="portfolio-categories">
											<ul class="orange">
												<?php if(get_post_meta($post->ID, 'pyre_address', true)): ?>
												<li><?php echo get_post_meta($post->ID, 'pyre_address', true); ?></li>
												<?php endif; ?>
												<?php if(get_post_meta($post->ID, 'pyre_phone', true)): ?>
												<li>Phone: <?php echo get_post_meta($post->ID, 'pyre_phone', true); ?></li>
												<?php endif; ?>
												<?php if(get_post_meta($post->ID, 'pyre_email', true)): ?>
												<li>Email: <a href="mailto:<?php echo get_post_meta($post->ID, 'pyre_email', true); ?>"><?php echo get_post_meta($post->ID, 'pyre_email', true); ?></a></li>
												<?php endif; ?>
											</ul>
										</div>
									</div>
									<form action="" method="post">
										<?php if(isset($hasError)) { //If errors are found ?>
											<p class="error"><?php _e("Por favor, verifique se você preencheu todos os campos corretamente.", 'Crucio'); ?></p>
										<?php } ?>

										<?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
											<p class="success-heading"><strong><?php _e('Email enviado com sucesso!', 'Crucio'); ?></strong></p>
											<p class="success"><?php _e('Obrigado', 'Crucio'); ?> <strong><?php echo $name;?></strong> <?php _e('por entrar em contato! Seu email foi enviado com sucesso e em breve será respondido.', 'Crucio'); ?></p>
										<?php } ?>

										<div id="comment-input">

											<input type="text" name="contact_name" id="author" value="<?php _e('Nome (obrigatório)', 'Crucio'); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> class="input-name" />

											<input type="text" name="email" id="email" value="<?php _e('Email (obrigatório)', 'Crucio'); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> class="input-email"  />
																			
										</div>
										
										<div id="comment-textarea">
											
											<textarea name="comment" id="comment" cols="39" rows="4" tabindex="4" class="textarea-comment"><?php _e('Mensagem ...', 'Crucio'); ?></textarea>
										
										</div>
										
										<div id="comment-submit">
										
											<p><div class="button <?php echo of_get_option('skin', 'orange'); ?>"><input name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Enviar', 'Crucio'); ?>" class="comment-submit" /></div></p>
											
										</div>
									</form>
								</div>
							</div>
							<?php endwhile; ?>
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>