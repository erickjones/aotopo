			</div>
			<div id="footer">
				<div class="copy">
					<div class="holder">
						<a class="link-top" href="#"><?php _e('Voltar "Aotopo"', 'Crucio'); ?></a>
						<p><?php echo of_get_option('copyright', '&copy; 2009 - 2013 Aotopo | Rio de Janeiro'); ?></p>
						<a href="https://plus.google.com/116136392359275426287" rel="publisher"><span class="gPlus-connect-link">Encontre-nos no Google+</span></a>

					</div>
				</div>
				<div id="rocket"></div>
			</div>
		</div>
		<?php wp_footer(); ?>
		<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
		<script src="http://s7.addthis.com/js/300/addthis_widget.js"></script>
	</body>
</html>