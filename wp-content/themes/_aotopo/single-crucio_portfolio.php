<?php get_header(); ?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">
							<?php while(have_posts()): the_post(); ?>
							<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">
								<div class="post" style="padding: 0;">
									<?php if(has_post_thumbnail()): ?>
									<div class="featured-image">
										<?php the_post_thumbnail('single-img'); ?>
									</div>
									<?php endif; ?>
									<h3><?php the_title(); ?></h3>
									<?php if(get_post_meta($post->ID, 'pyre_subtitle', true)): ?>
									<div class="meta"><?php echo get_post_meta($post->ID, 'pyre_subtitle', true); ?></div>
									<?php endif; ?>
									<div class="post-content portfolio-content">
										<div class="actual-portfolio-content">
											<?php the_content(); ?>
										</div>
										<?php
										$portfolio_category = get_terms('portfolio_category');
										if($portfolio_category):
										?>
										<div class="portfolio-categories">
											<h3><?php _e('Things We Did', 'Crucio'); ?></h3>
											<ul class="orange">
												<?php foreach($portfolio_category as $portfolio_cat): ?>
												<li><?php echo $portfolio_cat->name; ?></li>
												<?php endforeach; ?>
											</ul>
											<?php if(get_post_meta($post->ID, 'pyre_project_link', true)): ?>
											<div class="button <?php echo of_get_option('skin', 'orange'); ?>"><a href="<?php echo get_post_meta($post->ID, 'pyre_project_link', true); ?>"><?php _e('Visit Website', 'Crucio'); ?></a></div>
											<?php endif; ?>
										</div>
										<?php endif; ?>
									</div>
									<?php wp_reset_query(); ?>
									<?php comments_template(); ?>
								</div>
							</div>
							<?php endwhile; ?>
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>