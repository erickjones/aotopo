<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 * 
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet (lowercase and without spaces)
	$themename = get_theme_data(STYLESHEETPATH . '/style.css');
	$themename = $themename['Name'];
	$themename = preg_replace("/\W/", "", strtolower($themename) );
	
	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);
	
	// echo $themename;
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */

function optionsframework_options() {
	
	
	// Pull all the categories into an array
	$options_categories = array();  
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
    	$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all the pages into an array
	$options_pages = array();  
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
    	$options_pages[$page->ID] = $page->post_title;
	}
		
	// If using image radio buttons, define a directory path
	$imagepath =  get_bloginfo('stylesheet_directory') . '/images/';
		
	$options = array();
		
	$options[] = array( "name" => "Basic Settings",
						"type" => "heading");
	
	$options[] = array( "name" => "Header Banner Ad Code",
						"desc" => "",
						"id" => "header_ad",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Portfolio Items Per Page",
						"desc" => "",
						"id" => "portfolio_items",
						"std" => "10",
						"type" => "text");

	$options[] = array( "name" => "Gallery Items Per Page",
						"desc" => "",
						"id" => "gallery_items",
						"std" => "10",
						"type" => "text");

	$options[] = array( "name" => "Copyright (Left)",
						"desc" => "",
						"id" => "copyright",
						"std" => "&copy; 2012 Crucio | All Rights Reserved",
						"type" => "textarea");

	$options[] = array( "name" => "Copyright (Right)",
						"desc" => "",
						"id" => "copyright_2",
						"std" => 'Powered by <a href="http://wordpress.org">WordPress</a>  |  <a href="http://theme-fusion.com">Theme Fusion</a>',
						"type" => "textarea");

	$options[] = array( "name" => "Feedburner Link",
						"desc" => "",
						"id" => "feedburner_link",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Feedburner Email Subscribe Link",
						"desc" => "",
						"id" => "feedburner_email_link",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Appearance Settings",
						"type" => "heading");

	$options[] = array( "name" => "Logo",
						"desc" => "",
						"id" => "logo",
						"type" => "upload",
						"std" => get_bloginfo('template_directory') . "/images/logo.png");


	$options[] = array( "name" => "Color Scheme",
						"desc" => "",
						"id" => "skin",
						"std" => "orange",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('orange' => 'Orange', 'red' => 'Red', 'crimson' => 'Crimson', 'brown' => 'Brown', 'tan' => 'Tan', 'steel' => 'Steel', 'green' => 'Green', 'babyblue' => 'Baby Blue'));

	$options[] = array(
		'name' => "Background Pattern",
		'desc' => "",
		'id' => "pattern",
		'std' => "pattern_1",
		'type' => "images",
		'options' => array(
			'pattern_1' => get_bloginfo('template_directory') . '/images/patterns/pattern_1.png',
			'pattern_2' => get_bloginfo('template_directory') . '/images/patterns/pattern_2.png',
			'pattern_3' => get_bloginfo('template_directory') . '/images/patterns/pattern_3.png',
			'pattern_4' => get_bloginfo('template_directory') . '/images/patterns/pattern_4.png',
			'pattern_5' => get_bloginfo('template_directory') . '/images/patterns/pattern_5.png',
			'pattern_6' => get_bloginfo('template_directory') . '/images/patterns/pattern_6.png',
			'pattern_7' => get_bloginfo('template_directory') . '/images/patterns/pattern_7.png',
			'pattern_8' => get_bloginfo('template_directory') . '/images/patterns/pattern_8.png',
			'pattern_9' => get_bloginfo('template_directory') . '/images/patterns/pattern_9.png',
			'pattern_10' => get_bloginfo('template_directory') . '/images/patterns/pattern_10.png',
			)
	);

	$options[] = array( "name" => "Background",
						"desc" => "",
						"id" => "background",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Background Repeat",
						"desc" => "",
						"id" => "background_repeat",
						"std" => "repeat",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('no-repeat' => 'no-repeat', 'repeat' => 'repeat', 'repeat-x' => 'repeat-x', 'repeat-y' => 'repeat-y'));

	$options[] = array( "name" => "Sidebar Position",
						"desc" => "",
						"id" => "sidebar_position",
						"std" => "right",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('right' => 'Right', 'left' => 'Left'));

	$options[] = array( "name" => "Font",
						"desc" => "Select a google font",
						"id" => "google_font",
						"std" => "PT Sans",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array(
							"Abel" => "Abel",
							"Abril Fatface" => "Abril Fatface",
							"Aclonica" => "Aclonica",
							"Acme" => "Acme",
							"Actor" => "Actor",
							"Adamina" => "Adamina",
							"Advent Pro" => "Advent Pro",
							"Aguafina Script" => "Aguafina Script",
							"Aladin" => "Aladin",
							"Aldrich" => "Aldrich",
							"Alegreya" => "Alegreya",
							"Alegreya SC" => "Alegreya SC",
							"Alex Brush" => "Alex Brush",
							"Alfa Slab One" => "Alfa Slab One",
							"Alice" => "Alice",
							"Alike" => "Alike",
							"Alike Angular" => "Alike Angular",
							"Allan" => "Allan",
							"Allerta" => "Allerta",
							"Allerta Stencil" => "Allerta Stencil",
							"Allura" => "Allura",
							"Almendra" => "Almendra",
							"Almendra SC" => "Almendra SC",
							"Amaranth" => "Amaranth",
							"Amatic SC" => "Amatic SC",
							"Amethysta" => "Amethysta",
							"Andada" => "Andada",
							"Andika" => "Andika",
							"Angkor" => "Angkor",
							"Annie Use Your Telescope" => "Annie Use Your Telescope",
							"Anonymous Pro" => "Anonymous Pro",
							"Antic" => "Antic",
							"Antic Didone" => "Antic Didone",
							"Antic Slab" => "Antic Slab",
							"Anton" => "Anton",
							"Arapey" => "Arapey",
							"Arbutus" => "Arbutus",
							"Architects Daughter" => "Architects Daughter",
							"Arimo" => "Arimo",
							"Arizonia" => "Arizonia",
							"Armata" => "Armata",
							"Artifika" => "Artifika",
							"Arvo" => "Arvo",
							"Asap" => "Asap",
							"Asset" => "Asset",
							"Astloch" => "Astloch",
							"Asul" => "Asul",
							"Atomic Age" => "Atomic Age",
							"Aubrey" => "Aubrey",
							"Audiowide" => "Audiowide",
							"Average" => "Average",
							"Averia Gruesa Libre" => "Averia Gruesa Libre",
							"Averia Libre" => "Averia Libre",
							"Averia Sans Libre" => "Averia Sans Libre",
							"Averia Serif Libre" => "Averia Serif Libre",
							"Bad Script" => "Bad Script",
							"Balthazar" => "Balthazar",
							"Bangers" => "Bangers",
							"Basic" => "Basic",
							"Battambang" => "Battambang",
							"Baumans" => "Baumans",
							"Bayon" => "Bayon",
							"Belgrano" => "Belgrano",
							"Belleza" => "Belleza",
							"Bentham" => "Bentham",
							"Berkshire Swash" => "Berkshire Swash",
							"Bevan" => "Bevan",
							"Bigshot One" => "Bigshot One",
							"Bilbo" => "Bilbo",
							"Bilbo Swash Caps" => "Bilbo Swash Caps",
							"Bitter" => "Bitter",
							"Black Ops One" => "Black Ops One",
							"Bokor" => "Bokor",
							"Bonbon" => "Bonbon",
							"Boogaloo" => "Boogaloo",
							"Bowlby One" => "Bowlby One",
							"Bowlby One SC" => "Bowlby One SC",
							"Brawler" => "Brawler",
							"Bree Serif" => "Bree Serif",
							"Bubblegum Sans" => "Bubblegum Sans",
							"Buda" => "Buda",
							"Buenard" => "Buenard",
							"Butcherman" => "Butcherman",
							"Butterfly Kids" => "Butterfly Kids",
							"Cabin" => "Cabin",
							"Cabin Condensed" => "Cabin Condensed",
							"Cabin Sketch" => "Cabin Sketch",
							"Caesar Dressing" => "Caesar Dressing",
							"Cagliostro" => "Cagliostro",
							"Calligraffitti" => "Calligraffitti",
							"Cambo" => "Cambo",
							"Candal" => "Candal",
							"Cantarell" => "Cantarell",
							"Cantata One" => "Cantata One",
							"Cardo" => "Cardo",
							"Carme" => "Carme",
							"Carter One" => "Carter One",
							"Caudex" => "Caudex",
							"Cedarville Cursive" => "Cedarville Cursive",
							"Ceviche One" => "Ceviche One",
							"Changa One" => "Changa One",
							"Chango" => "Chango",
							"Chau Philomene One" => "Chau Philomene One",
							"Chelsea Market" => "Chelsea Market",
							"Chenla" => "Chenla",
							"Cherry Cream Soda" => "Cherry Cream Soda",
							"Chewy" => "Chewy",
							"Chicle" => "Chicle",
							"Chivo" => "Chivo",
							"Coda" => "Coda",
							"Coda Caption" => "Coda Caption",
							"Codystar" => "Codystar",
							"Comfortaa" => "Comfortaa",
							"Coming Soon" => "Coming Soon",
							"Concert One" => "Concert One",
							"Condiment" => "Condiment",
							"Content" => "Content",
							"Contrail One" => "Contrail One",
							"Convergence" => "Convergence",
							"Cookie" => "Cookie",
							"Copse" => "Copse",
							"Corben" => "Corben",
							"Cousine" => "Cousine",
							"Coustard" => "Coustard",
							"Covered By Your Grace" => "Covered By Your Grace",
							"Crafty Girls" => "Crafty Girls",
							"Creepster" => "Creepster",
							"Crete Round" => "Crete Round",
							"Crimson Text" => "Crimson Text",
							"Crushed" => "Crushed",
							"Cuprum" => "Cuprum",
							"Cutive" => "Cutive",
							"Damion" => "Damion",
							"Dancing Script" => "Dancing Script",
							"Dangrek" => "Dangrek",
							"Dawning of a New Day" => "Dawning of a New Day",
							"Days One" => "Days One",
							"Delius" => "Delius",
							"Delius Swash Caps" => "Delius Swash Caps",
							"Delius Unicase" => "Delius Unicase",
							"Della Respira" => "Della Respira",
							"Devonshire" => "Devonshire",
							"Didact Gothic" => "Didact Gothic",
							"Diplomata" => "Diplomata",
							"Diplomata SC" => "Diplomata SC",
							"Doppio One" => "Doppio One",
							"Dorsa" => "Dorsa",
							"Dosis" => "Dosis",
							"Dr Sugiyama" => "Dr Sugiyama",
							"Droid Sans" => "Droid Sans",
							"Droid Sans Mono" => "Droid Sans Mono",
							"Droid Serif" => "Droid Serif",
							"Duru Sans" => "Duru Sans",
							"Dynalight" => "Dynalight",
							"EB Garamond" => "EB Garamond",
							"Eater" => "Eater",
							"Economica" => "Economica",
							"Electrolize" => "Electrolize",
							"Emblema One" => "Emblema One",
							"Emilys Candy" => "Emilys Candy",
							"Engagement" => "Engagement",
							"Enriqueta" => "Enriqueta",
							"Erica One" => "Erica One",
							"Esteban" => "Esteban",
							"Euphoria Script" => "Euphoria Script",
							"Ewert" => "Ewert",
							"Exo" => "Exo",
							"Expletus Sans" => "Expletus Sans",
							"Fanwood Text" => "Fanwood Text",
							"Fascinate" => "Fascinate",
							"Fascinate Inline" => "Fascinate Inline",
							"Federant" => "Federant",
							"Federo" => "Federo",
							"Felipa" => "Felipa",
							"Fjord One" => "Fjord One",
							"Flamenco" => "Flamenco",
							"Flavors" => "Flavors",
							"Fondamento" => "Fondamento",
							"Fontdiner Swanky" => "Fontdiner Swanky",
							"Forum" => "Forum",
							"Francois One" => "Francois One",
							"Fredericka the Great" => "Fredericka the Great",
							"Fredoka One" => "Fredoka One",
							"Freehand" => "Freehand",
							"Fresca" => "Fresca",
							"Frijole" => "Frijole",
							"Fugaz One" => "Fugaz One",
							"GFS Didot" => "GFS Didot",
							"GFS Neohellenic" => "GFS Neohellenic",
							"Galdeano" => "Galdeano",
							"Gentium Basic" => "Gentium Basic",
							"Gentium Book Basic" => "Gentium Book Basic",
							"Geo" => "Geo",
							"Geostar" => "Geostar",
							"Geostar Fill" => "Geostar Fill",
							"Germania One" => "Germania One",
							"Give You Glory" => "Give You Glory",
							"Glass Antiqua" => "Glass Antiqua",
							"Glegoo" => "Glegoo",
							"Gloria Hallelujah" => "Gloria Hallelujah",
							"Goblin One" => "Goblin One",
							"Gochi Hand" => "Gochi Hand",
							"Gorditas" => "Gorditas",
							"Goudy Bookletter 1911" => "Goudy Bookletter 1911",
							"Graduate" => "Graduate",
							"Gravitas One" => "Gravitas One",
							"Great Vibes" => "Great Vibes",
							"Gruppo" => "Gruppo",
							"Gudea" => "Gudea",
							"Habibi" => "Habibi",
							"Hammersmith One" => "Hammersmith One",
							"Handlee" => "Handlee",
							"Hanuman" => "Hanuman",
							"Happy Monkey" => "Happy Monkey",
							"Henny Penny" => "Henny Penny",
							"Herr Von Muellerhoff" => "Herr Von Muellerhoff",
							"Holtwood One SC" => "Holtwood One SC",
							"Homemade Apple" => "Homemade Apple",
							"Homenaje" => "Homenaje",
							"IM Fell DW Pica" => "IM Fell DW Pica",
							"IM Fell DW Pica SC" => "IM Fell DW Pica SC",
							"IM Fell Double Pica" => "IM Fell Double Pica",
							"IM Fell Double Pica SC" => "IM Fell Double Pica SC",
							"IM Fell English" => "IM Fell English",
							"IM Fell English SC" => "IM Fell English SC",
							"IM Fell French Canon" => "IM Fell French Canon",
							"IM Fell French Canon SC" => "IM Fell French Canon SC",
							"IM Fell Great Primer" => "IM Fell Great Primer",
							"IM Fell Great Primer SC" => "IM Fell Great Primer SC",
							"Iceberg" => "Iceberg",
							"Iceland" => "Iceland",
							"Imprima" => "Imprima",
							"Inconsolata" => "Inconsolata",
							"Inder" => "Inder",
							"Indie Flower" => "Indie Flower",
							"Inika" => "Inika",
							"Irish Grover" => "Irish Grover",
							"Istok Web" => "Istok Web",
							"Italiana" => "Italiana",
							"Italianno" => "Italianno",
							"Jim Nightshade" => "Jim Nightshade",
							"Jockey One" => "Jockey One",
							"Jolly Lodger" => "Jolly Lodger",
							"Josefin Sans" => "Josefin Sans",
							"Josefin Slab" => "Josefin Slab",
							"Judson" => "Judson",
							"Julee" => "Julee",
							"Junge" => "Junge",
							"Jura" => "Jura",
							"Just Another Hand" => "Just Another Hand",
							"Just Me Again Down Here" => "Just Me Again Down Here",
							"Kameron" => "Kameron",
							"Karla" => "Karla",
							"Kaushan Script" => "Kaushan Script",
							"Kelly Slab" => "Kelly Slab",
							"Kenia" => "Kenia",
							"Khmer" => "Khmer",
							"Knewave" => "Knewave",
							"Kotta One" => "Kotta One",
							"Koulen" => "Koulen",
							"Kranky" => "Kranky",
							"Kreon" => "Kreon",
							"Kristi" => "Kristi",
							"Krona One" => "Krona One",
							"La Belle Aurore" => "La Belle Aurore",
							"Lancelot" => "Lancelot",
							"Lato" => "Lato",
							"League Script" => "League Script",
							"Leckerli One" => "Leckerli One",
							"Ledger" => "Ledger",
							"Lekton" => "Lekton",
							"Lemon" => "Lemon",
							"Lilita One" => "Lilita One",
							"Limelight" => "Limelight",
							"Linden Hill" => "Linden Hill",
							"Lobster" => "Lobster",
							"Lobster Two" => "Lobster Two",
							"Londrina Outline" => "Londrina Outline",
							"Londrina Shadow" => "Londrina Shadow",
							"Londrina Sketch" => "Londrina Sketch",
							"Londrina Solid" => "Londrina Solid",
							"Lora" => "Lora",
							"Love Ya Like A Sister" => "Love Ya Like A Sister",
							"Loved by the King" => "Loved by the King",
							"Lovers Quarrel" => "Lovers Quarrel",
							"Luckiest Guy" => "Luckiest Guy",
							"Lusitana" => "Lusitana",
							"Lustria" => "Lustria",
							"Macondo" => "Macondo",
							"Macondo Swash Caps" => "Macondo Swash Caps",
							"Magra" => "Magra",
							"Maiden Orange" => "Maiden Orange",
							"Mako" => "Mako",
							"Marck Script" => "Marck Script",
							"Marko One" => "Marko One",
							"Marmelad" => "Marmelad",
							"Marvel" => "Marvel",
							"Mate" => "Mate",
							"Mate SC" => "Mate SC",
							"Maven Pro" => "Maven Pro",
							"Meddon" => "Meddon",
							"MedievalSharp" => "MedievalSharp",
							"Medula One" => "Medula One",
							"Megrim" => "Megrim",
							"Merienda One" => "Merienda One",
							"Merriweather" => "Merriweather",
							"Metal" => "Metal",
							"Metamorphous" => "Metamorphous",
							"Metrophobic" => "Metrophobic",
							"Michroma" => "Michroma",
							"Miltonian" => "Miltonian",
							"Miltonian Tattoo" => "Miltonian Tattoo",
							"Miniver" => "Miniver",
							"Miss Fajardose" => "Miss Fajardose",
							"Modern Antiqua" => "Modern Antiqua",
							"Molengo" => "Molengo",
							"Monofett" => "Monofett",
							"Monoton" => "Monoton",
							"Monsieur La Doulaise" => "Monsieur La Doulaise",
							"Montaga" => "Montaga",
							"Montez" => "Montez",
							"Montserrat" => "Montserrat",
							"Moul" => "Moul",
							"Moulpali" => "Moulpali",
							"Mountains of Christmas" => "Mountains of Christmas",
							"Mr Bedfort" => "Mr Bedfort",
							"Mr Dafoe" => "Mr Dafoe",
							"Mr De Haviland" => "Mr De Haviland",
							"Mrs Saint Delafield" => "Mrs Saint Delafield",
							"Mrs Sheppards" => "Mrs Sheppards",
							"Muli" => "Muli",
							"Mystery Quest" => "Mystery Quest",
							"Neucha" => "Neucha",
							"Neuton" => "Neuton",
							"News Cycle" => "News Cycle",
							"Niconne" => "Niconne",
							"Nixie One" => "Nixie One",
							"Nobile" => "Nobile",
							"Nokora" => "Nokora",
							"Norican" => "Norican",
							"Nosifer" => "Nosifer",
							"Nothing You Could Do" => "Nothing You Could Do",
							"Noticia Text" => "Noticia Text",
							"Nova Cut" => "Nova Cut",
							"Nova Flat" => "Nova Flat",
							"Nova Mono" => "Nova Mono",
							"Nova Oval" => "Nova Oval",
							"Nova Round" => "Nova Round",
							"Nova Script" => "Nova Script",
							"Nova Slim" => "Nova Slim",
							"Nova Square" => "Nova Square",
							"Numans" => "Numans",
							"Nunito" => "Nunito",
							"Odor Mean Chey" => "Odor Mean Chey",
							"Old Standard TT" => "Old Standard TT",
							"Oldenburg" => "Oldenburg",
							"Oleo Script" => "Oleo Script",
							"Open Sans" => "Open Sans",
							"Open Sans Condensed" => "Open Sans Condensed",
							"Orbitron" => "Orbitron",
							"Original Surfer" => "Original Surfer",
							"Oswald" => "Oswald",
							"Over the Rainbow" => "Over the Rainbow",
							"Overlock" => "Overlock",
							"Overlock SC" => "Overlock SC",
							"Ovo" => "Ovo",
							"Oxygen" => "Oxygen",
							"PT Mono" => "PT Mono",
							"PT Sans" => "PT Sans",
							"PT Sans Caption" => "PT Sans Caption",
							"PT Sans Narrow" => "PT Sans Narrow",
							"PT Serif" => "PT Serif",
							"PT Serif Caption" => "PT Serif Caption",
							"Pacifico" => "Pacifico",
							"Parisienne" => "Parisienne",
							"Passero One" => "Passero One",
							"Passion One" => "Passion One",
							"Patrick Hand" => "Patrick Hand",
							"Patua One" => "Patua One",
							"Paytone One" => "Paytone One",
							"Permanent Marker" => "Permanent Marker",
							"Petrona" => "Petrona",
							"Philosopher" => "Philosopher",
							"Piedra" => "Piedra",
							"Pinyon Script" => "Pinyon Script",
							"Plaster" => "Plaster",
							"Play" => "Play",
							"Playball" => "Playball",
							"Playfair Display" => "Playfair Display",
							"Podkova" => "Podkova",
							"Poiret One" => "Poiret One",
							"Poller One" => "Poller One",
							"Poly" => "Poly",
							"Pompiere" => "Pompiere",
							"Pontano Sans" => "Pontano Sans",
							"Port Lligat Sans" => "Port Lligat Sans",
							"Port Lligat Slab" => "Port Lligat Slab",
							"Prata" => "Prata",
							"Preahvihear" => "Preahvihear",
							"Press Start 2P" => "Press Start 2P",
							"Princess Sofia" => "Princess Sofia",
							"Prociono" => "Prociono",
							"Prosto One" => "Prosto One",
							"Puritan" => "Puritan",
							"Quantico" => "Quantico",
							"Quattrocento" => "Quattrocento",
							"Quattrocento Sans" => "Quattrocento Sans",
							"Questrial" => "Questrial",
							"Quicksand" => "Quicksand",
							"Qwigley" => "Qwigley",
							"Radley" => "Radley",
							"Raleway" => "Raleway",
							"Rammetto One" => "Rammetto One",
							"Rancho" => "Rancho",
							"Rationale" => "Rationale",
							"Redressed" => "Redressed",
							"Reenie Beanie" => "Reenie Beanie",
							"Revalia" => "Revalia",
							"Ribeye" => "Ribeye",
							"Ribeye Marrow" => "Ribeye Marrow",
							"Righteous" => "Righteous",
							"Rochester" => "Rochester",
							"Rock Salt" => "Rock Salt",
							"Rokkitt" => "Rokkitt",
							"Ropa Sans" => "Ropa Sans",
							"Rosario" => "Rosario",
							"Rosarivo" => "Rosarivo",
							"Rouge Script" => "Rouge Script",
							"Ruda" => "Ruda",
							"Ruge Boogie" => "Ruge Boogie",
							"Ruluko" => "Ruluko",
							"Ruslan Display" => "Ruslan Display",
							"Russo One" => "Russo One",
							"Ruthie" => "Ruthie",
							"Sail" => "Sail",
							"Salsa" => "Salsa",
							"Sancreek" => "Sancreek",
							"Sansita One" => "Sansita One",
							"Sarina" => "Sarina",
							"Satisfy" => "Satisfy",
							"Schoolbell" => "Schoolbell",
							"Seaweed Script" => "Seaweed Script",
							"Sevillana" => "Sevillana",
							"Shadows Into Light" => "Shadows Into Light",
							"Shadows Into Light Two" => "Shadows Into Light Two",
							"Shanti" => "Shanti",
							"Share" => "Share",
							"Shojumaru" => "Shojumaru",
							"Short Stack" => "Short Stack",
							"Siemreap" => "Siemreap",
							"Sigmar One" => "Sigmar One",
							"Signika" => "Signika",
							"Signika Negative" => "Signika Negative",
							"Simonetta" => "Simonetta",
							"Sirin Stencil" => "Sirin Stencil",
							"Six Caps" => "Six Caps",
							"Slackey" => "Slackey",
							"Smokum" => "Smokum",
							"Smythe" => "Smythe",
							"Sniglet" => "Sniglet",
							"Snippet" => "Snippet",
							"Sofia" => "Sofia",
							"Sonsie One" => "Sonsie One",
							"Sorts Mill Goudy" => "Sorts Mill Goudy",
							"Special Elite" => "Special Elite",
							"Spicy Rice" => "Spicy Rice",
							"Spinnaker" => "Spinnaker",
							"Spirax" => "Spirax",
							"Squada One" => "Squada One",
							"Stardos Stencil" => "Stardos Stencil",
							"Stint Ultra Condensed" => "Stint Ultra Condensed",
							"Stint Ultra Expanded" => "Stint Ultra Expanded",
							"Stoke" => "Stoke",
							"Sue Ellen Francisco" => "Sue Ellen Francisco",
							"Sunshiney" => "Sunshiney",
							"Supermercado One" => "Supermercado One",
							"Suwannaphum" => "Suwannaphum",
							"Swanky and Moo Moo" => "Swanky and Moo Moo",
							"Syncopate" => "Syncopate",
							"Tangerine" => "Tangerine",
							"Taprom" => "Taprom",
							"Telex" => "Telex",
							"Tenor Sans" => "Tenor Sans",
							"The Girl Next Door" => "The Girl Next Door",
							"Tienne" => "Tienne",
							"Tinos" => "Tinos",
							"Titan One" => "Titan One",
							"Trade Winds" => "Trade Winds",
							"Trocchi" => "Trocchi",
							"Trochut" => "Trochut",
							"Trykker" => "Trykker",
							"Tulpen One" => "Tulpen One",
							"Ubuntu" => "Ubuntu",
							"Ubuntu Condensed" => "Ubuntu Condensed",
							"Ubuntu Mono" => "Ubuntu Mono",
							"Ultra" => "Ultra",
							"Uncial Antiqua" => "Uncial Antiqua",
							"UnifrakturCook" => "UnifrakturCook",
							"UnifrakturMaguntia" => "UnifrakturMaguntia",
							"Unkempt" => "Unkempt",
							"Unlock" => "Unlock",
							"Unna" => "Unna",
							"VT323" => "VT323",
							"Varela" => "Varela",
							"Varela Round" => "Varela Round",
							"Vast Shadow" => "Vast Shadow",
							"Vibur" => "Vibur",
							"Vidaloka" => "Vidaloka",
							"Viga" => "Viga",
							"Voces" => "Voces",
							"Volkhov" => "Volkhov",
							"Vollkorn" => "Vollkorn",
							"Voltaire" => "Voltaire",
							"Waiting for the Sunrise" => "Waiting for the Sunrise",
							"Wallpoet" => "Wallpoet",
							"Walter Turncoat" => "Walter Turncoat",
							"Wellfleet" => "Wellfleet",
							"Wire One" => "Wire One",
							"Yanone Kaffeesatz" => "Yanone Kaffeesatz",
							"Yellowtail" => "Yellowtail",
							"Yeseva One" => "Yeseva One",
							"Yesteryear" => "Yesteryear",
							"Zeyada" => "Zeyada",
						));

	$options[] = array( "name" => "Content Settings",
						"type" => "heading");

	$options[] = array( "name" => "Add read more link after post excerpt?",
						"desc" => "",
						"id" => "read_more",
						"std" => "yes",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('yes' => 'Yes', 'no' => 'No'));

	$options[] = array( "name" => "Show post date?",
						"desc" => "",
						"id" => "post_date",
						"std" => "yes",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('yes' => 'Yes', 'no' => 'No'));

	$options[] = array( "name" => "Show post date?",
						"desc" => "",
						"id" => "post_date",
						"std" => "yes",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('yes' => 'Yes', 'no' => 'No'));

	$options[] = array( "name" => "Show post author on post meta?",
						"desc" => "",
						"id" => "post_author_meta",
						"std" => "yes",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('yes' => 'Yes', 'no' => 'No'));

	$options[] = array( "name" => "Show number of comments on post meta?",
						"desc" => "",
						"id" => "post_comments_meta",
						"std" => "yes",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('yes' => 'Yes', 'no' => 'No'));

	$options[] = array( "name" => "Comments",
						"desc" => "",
						"id" => "comments",
						"std" => "enable",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array('enable' => 'Enable', 'disable' => 'Disable'));

	$options[] = array( "name" => "Slider Settings",
						"type" => "heading");

	$options[] = array( "name" => "Slider type",
						"desc" => "",
						"id" => "slider_type",
						"std" => "half-width",
						"type" => "select",
						"class" => "small", //mini, tiny, small
						"options" => array('full-width' => 'Full Width with Thumbnails', 'full-width-text' => 'Full Width with Text', 'half-width' => 'Half Width with Thumbnails', 'half-width-text' => 'Half Width with Text', 'no' => 'Disable Slider'));

	$options[] = array( "name" => "Slider animation speed",
						"desc" => "",
						"id" => "slider_animation_speed",
						"std" => "500",
						"type" => "text",
						"class" => "mini");

	$options[] = array( "name" => "Slide speed",
						"desc" => "",
						"id" => "slide_speed",
						"std" => "3000",
						"type" => "text",
						"class" => "mini");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_1",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_1_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_1",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Slide 2",
						"type" => "info");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_2",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_2",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_2_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_2",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Slide 3",
						"type" => "info");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_3",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_3",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_3_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_3",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Slide 4",
						"type" => "info");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_4",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_4",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_4_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_4",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Slide 5",
						"type" => "info");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_5",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_5",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_5_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_5",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Slide 6 (Full Width Only)",
						"type" => "info");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_6",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_6",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_6_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_6",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Slide 7 (Full Width Only)",
						"type" => "info");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_7",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_7",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_7_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_7",
						"std" => "",
						"type" => "textarea");

	$options[] = array( "name" => "Slide 8 (Full Width Only)",
						"type" => "info");

	$options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slide_image_8",
						"type" => "upload",
						"std" => "");

	$options[] = array( "name" => "Link",
						"desc" => "",
						"id" => "slide_link_8",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Title",
						"desc" => "",
						"id" => "slide_title_8_1",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Description",
						"desc" => "",
						"id" => "slide_desc_8",
						"std" => "",
						"type" => "textarea");

	return $options;
}