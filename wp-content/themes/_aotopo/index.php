<?php get_header(); ?>
<div class="holder">

	<?php if(is_home() && !is_paged()){ ?>
			<div id='destaqueHome'>
				<div class="textoDestaqueHome">
					<h3>Aotopo - Webdesign e Marketing Digital</h3>
					<p>Inbound Marketing, Design e Desenvolvimento são as palavras-chave do nosso <strong>negócio</strong>. Histórias, Artigos e Start-up, são aquelas que mais combinam <strong>com</strong> nossa <strong>filosofia</strong>.<br /><br />
<strong>Conheça a nossa <a href="http://www.aotopo.com.br/experiencia/">experiência</a> e nossos <a href="http://www.aotopo.com.br/servicos/">serviços</a></strong>!</p>
                </div>
			</div>
	<?php } ?>

	<div class="frame">

		<div class="main-content">

			<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">


				<div class="heading"><h2>Últimas postagens</h2></div>

				<?php while(have_posts()): the_post(); ?>

				<div class="post posts-index" style="padding: 0;">

					<?php if(has_post_thumbnail()): ?>
					<div class="featured-image">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('single-img'); ?>
						</a>
					</div>
				<?php endif; ?>

				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

				<span class="autor">Por: <?php the_author_posts_link(); ?></span>

				<div class="meta">
					<span class="data"><?php the_time('j \d\e F'); ?></span>
					<span class="tags">
						<?php
						$categories = get_the_category();
						$seperator = '  ';
						$output = '';
						if($categories){
							foreach($categories as $category) {
								$output .= '<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "Ver todos os posts em %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.', ';
							}
							echo trim($output, $seperator);
						}
						?></span>
						<span class="comentarios"><?php comments_popup_link('Sem comentários', '1 Comentário', '% Comentários'); ?></span>
					</div>


					<div class="post-content">
						<p><?php echo string_limit_words(get_the_excerpt(), 100); ?> ...</p>
						<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>
						<br>
						<?php if(of_get_option('read_more', 'yes') == 'yes'): ?>
						<div class='button <?php echo of_get_option('skin', 'orange'); ?> read-more'><a href="<?php the_permalink(); ?>"><?php _e('Leia mais', 'Crucio'); ?></a></div>
					<?php endif; ?>
				</div>
			</div>

		<?php endwhile; ?>
		<?php kriesi_pagination($pages = '', $range = 2); ?>
	</div>
	<?php get_sidebar(); ?>
</div>
</div>
</div>
<?php get_footer(); ?>