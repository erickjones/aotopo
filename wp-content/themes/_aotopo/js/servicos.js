jQuery(document).ready(function($) {

	var servicosLinks = $(".myServiceIcon a").find("img");

	servicosLinks.each(function(){

		$(this).hover(
			function(){
				$(this).stop().animate({"margin-top": "-15px"}, 200);
			}, 
			function(){
				$(this).stop().animate({"margin-top": "0"}, 200);
			}
		);

	})

});