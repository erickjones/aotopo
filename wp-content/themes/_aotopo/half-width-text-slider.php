<?php
if(
	of_get_option('slide_image_1') ||
	of_get_option('slide_image_2') ||
	of_get_option('slide_image_3') ||
	of_get_option('slide_image_4') ||
	of_get_option('slide_image_5')
):
?>
<div class="main-gallery half-width-text-slider">
	<div class="gallery-holder flexslider">
		<ul class="slides">
			<?php
			$slides = array(1, 2, 3, 4, 5);
			foreach($slides as $slide_no):
				if(of_get_option('slide_image_'.$slide_no)):
			?>
			<li data-thumb="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo of_get_option('slide_image_'.$slide_no); ?>&amp;w=102&amp;h=39">
				<?php if(of_get_option('slide_link_'.$slide_no)): ?>
				<a href="<?php echo of_get_option('slide_link_'.$slide_no); ?>">
				<?php endif; ?>
				<img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo of_get_option('slide_image_'.$slide_no); ?>&amp;w=592&amp;h=286" alt="image description" width="592" height="286" />
				<?php if(of_get_option('slide_link_'.$slide_no)): ?>
				</a>
				<?php endif; ?>
				<?php if(of_get_option('slide_title_'.$slide_no.'_1') || of_get_option('slide_desc_'.$slide_no)): ?>
				<div class="text-box">
					<?php if(of_get_option('slide_title_'.$slide_no.'_1')): ?>
					<h1>
						<?php if(of_get_option('slide_link_'.$slide_no)): ?>
						<a href="<?php echo of_get_option('slide_link_'.$slide_no); ?>">
						<?php endif; ?>
						<?php echo of_get_option('slide_title_'.$slide_no.'_1'); ?>
						<?php if(of_get_option('slide_link_'.$slide_no)): ?>
						</a>
						<?php endif; ?>
					</h1>
					<?php endif; ?>
					<?php if(of_get_option('slide_desc_'.$slide_no)): ?>
					<p><?php echo of_get_option('slide_desc_'.$slide_no); ?></p>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</li>
			<?php endif; endforeach; ?>
		</ul>
	</div>
	<img class="decor" src="<?php bloginfo('template_directory'); ?>/images/bg-main-gallery.png" alt="decor" width="622" height="418" />
</div>
<?php endif; ?>