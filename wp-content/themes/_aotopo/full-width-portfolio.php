<?php
// Template Name: Full Width Portfolio
get_header(); ?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">
							<?php while(have_posts()): the_post(); ?>
							<div id="content" class="gallery full-width-gallery" style="width:100%;">
								<?php
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$args = array(
									'post_type' => 'crucio_portfolio',
									'paged' => $paged,
									'posts_per_page' => of_get_option('portfolio_items', '10'),
								);
								$gallery = new WP_Query($args);
								while($gallery->have_posts()): $gallery->the_post();
									$args = array(
									    'post_type' => 'attachment',
									    'numberposts' => '5',
									    'post_status' => null,
									    'post_parent' => $post->ID,
										'orderby' => 'menu_order',
										'order' => 'ASC',
									);
									$attachments = get_posts($args);
									if($attachments || has_post_thumbnail()):
								?>
								<script type="text/javascript">
								jQuery(document).ready(function($) {
									if($('.gallery-item-<?php echo $post->ID; ?> .multiple-images img').length == 1) {
										$('.gallery-item-<?php echo $post->ID; ?> .nav').hide();
									}
									$('.gallery-item-<?php echo $post->ID; ?> .multiple-images').cycle({
										fx: 'fade',
										timeout: 1000,
										slideResize: 0,
										next: '.gallery-item-<?php echo $post->ID; ?> .next',
										prev: '.gallery-item-<?php echo $post->ID; ?> .prev',
									    before: function(c, n, o) {
								        	$(c).css('position', 'absolute');
								        	$(n).css('position', 'relative');
								    	}
									});
								});
								</script>
								<div class="gallery-item gallery-item-<?php echo $post->ID; ?>">
									<div class="multiple-images-container">
										<div class="multiple-images">
										<?php if($attachments): foreach($attachments as $attachment): ?>
											<?php $attachment_image = wp_get_attachment_image_src($attachment->ID, 'gallery-img'); ?>
											<?php $full_image = wp_get_attachment_image_src($attachment->ID, 'full'); ?>
											<?php $attachment_data = wp_get_attachment_metadata($attachment->ID); ?>
											<a href="<?php the_permalink(); ?>" style="display: block;" class="single-home-image"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment->post_title; ?>" /></a>
										<?php endforeach; else: ?>
											<?php $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'gallery-img'); ?>
											<?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
											<?php $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id($post->ID)); ?>
											<a href="<?php the_permalink(); ?>" style="display: block;" class="single-home-image"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment->post_title; ?>" /></a>
										<?php endif; ?>
										</div>
										<div class="nav">
											<a class="prev" href="#">Prev</a>
											<a class="next" href="#">Prev</a>
										</div>
									</div>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php if(get_post_meta($post->ID, 'pyre_subtitle', true)): ?>
									<div class="meta"><?php echo get_post_meta($post->ID, 'pyre_subtitle', true); ?></div>
									<?php endif; ?>
								</div>
								<?php endif; endwhile; ?>
								<?php kriesi_pagination($gallery->max_num_pages, $range = 2); ?>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>