<?php
if(
	of_get_option('slide_image_1') ||
	of_get_option('slide_image_2') ||
	of_get_option('slide_image_3') ||
	of_get_option('slide_image_4') ||
	of_get_option('slide_image_5') ||
	of_get_option('slide_image_6') ||
	of_get_option('slide_image_7') ||
	of_get_option('slide_image_8') 
):
?>
<div class="main-gallery full-gallery full-width-text-slider">
	<div class="gallery-holder flexslider">
		<ul class="slides">
			<?php
			$slides = array(1, 2, 3, 4, 5, 6, 7, 8);
			foreach($slides as $slide_no):
				if(of_get_option('slide_image_'.$slide_no)):
			?>
			<li data-thumb="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo of_get_option('slide_image_'.$slide_no); ?>&amp;w=95&amp;h=40">
				<?php if(of_get_option('slide_link_'.$slide_no)): ?>
				<a href="<?php echo of_get_option('slide_link_'.$slide_no); ?>">
				<?php endif; ?>
				<img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo of_get_option('slide_image_'.$slide_no); ?>&amp;w=913&amp;h=346" alt="image description" width="913" height="346" />
				<?php if(of_get_option('slide_link_'.$slide_no)): ?>
				</a>
				<?php endif; ?>
				<?php if(of_get_option('slide_title_'.$slide_no.'_1') || of_get_option('slide_desc_'.$slide_no)): ?>
				<div class="text-box">
					<?php if(of_get_option('slide_title_'.$slide_no.'_1')): ?>
					<h1>
						<?php if(of_get_option('slide_link_'.$slide_no)): ?>
						<a href="<?php echo of_get_option('slide_link_'.$slide_no); ?>">
						<?php endif; ?>
						<?php echo of_get_option('slide_title_'.$slide_no.'_1'); ?>
						<?php if(of_get_option('slide_link_'.$slide_no)): ?>
						</a>
						<?php endif; ?>
					</h1>
					<?php endif; ?>
					<?php if(of_get_option('slide_desc_'.$slide_no)): ?>
					<p><?php echo of_get_option('slide_desc_'.$slide_no); ?></p>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</li>
			<?php endif; endforeach; ?>
		</ul>
	</div>
	<img class="decor" src="<?php bloginfo('template_directory'); ?>/images/bg-main-gallery-full.png" alt="decor" width="941" height="414" />
</div>
<?php endif; ?>