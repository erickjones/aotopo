<?php
/*
Template Name: servicos
*/
?>

<?php get_header(); ?>
<div class="holder">
	<div class="frame">
		<div class="main-content page-servicos">

			<?php while(have_posts()): the_post(); ?>

			<div id="content" style="width:100%;">
				
				<div class="heading">
					<?php if (function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?>
					<br>
				</div>

				<h3 class="fixPageTitle"><?php the_title(); ?></h3>
				
				<div class="post" style="padding:0;">	

					<div class="post-content servicos" style="background:none; min-height:400px">
						
						<?php the_content(); ?>

<div id="serviceIconsBox">

						<div class="myServiceIcon"><a href="web-design/"><img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign.png"?>" alt="Web Design"><p>Web Design</p></a></div>

<div class="myServiceIcon"><a href="inbound-marketing/"><img src="<?php echo get_bloginfo('template_directory')."/images/servicos-inbound.png"?>" alt="Inbound Marketing"><p>Inbound Marketing</p></a></div>

						<div class="myServiceIcon"><a href="parcerias/"><img src="<?php echo get_bloginfo('template_directory')."/images/servicos-startup.png"?>" alt="Inbound Marketing"><p>Parcerias</p></a></div>

</div>

					</div>
			</div>

			<?php endwhile; ?>

			<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>

		</div>
	</div>
</div>
<script src="<?php echo get_bloginfo('template_directory')."/js/jquery-1.8.2.js"?>"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/servicos.js"?>"></script>
<?php get_footer(); ?>