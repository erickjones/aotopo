<?php
function get_related_posts($post_id) {
	$query = new WP_Query();
    
    $args = '';

	$args = wp_parse_args($args, array(
		'showposts' => 10,
		'post__not_in' => array($post_id),
		'ignore_sticky_posts' => 1,
        'category__in' => wp_get_post_categories($post_id)
	));
	
	$query = new WP_Query($args);
	
  	return $query;
}

function kriesi_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination clearfix'>";
         //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'><span class='arrows'>&laquo;</span> First</a>";
         if($paged > 1) echo "<a href='".get_pagenum_link($paged - 1)."'><span class='page-prev'></span>".__('Anterior', 'Crucio')."</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>".__('Próxima', 'Crucio')."<span class='page-next'></span></a>";  
         //if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last <span class='arrows'>&raquo;</span></a>";
         echo "</div>\n";
     }
}

function string_limit_words($string, $word_limit)
{
	$words = explode(' ', $string, ($word_limit + 1));
	
	if(count($words) > $word_limit) {
		array_pop($words);
	}
	
	return implode(' ', $words);
}

function kriesi_breadcrumb() {

        echo '<ul class="breadcrumbs">';
        
        if ( !is_front_page() ) {
            echo '<li>'.__('Você está em', 'Crucio').':</li><li><a href="';
            echo home_url();
            echo '">';
            //bloginfo('name');
            echo "Home</a></li>";
        } else {
            echo "Últimas postagens";
        }
        
        if ( is_category() && !is_singular('crucio_portfolio')) {
            $category = get_the_category();
            $ID = $category[0]->cat_ID;

            $pcat = get_category_parents($ID, TRUE, '', FALSE );
            if(!is_wp_error($pcat)) echo '<li>'.$pcat.'</li>';
        }

        //if(is_single() || is_page()) { echo '<li>'.get_the_title().'</li>'; }

        if(is_single() || is_page()) {
            $category = get_the_category();
            $ID = $category[0]->cat_ID;
            $pcat = get_category_parents($ID, TRUE, '', FALSE );
            if(!is_wp_error($pcat)) echo '<li>'.$pcat.'</li>';
        }


        if(is_tag()){ echo '<li>'."Tag: ".single_tag_title('',FALSE).'</li>'; }
        if(is_404()){ echo '<li>'.__("404 - página não encontrada", 'Crucio').'</li>'; }
        if(is_search()){ echo '<li>'.__("Buscar", 'Crucio').'</li>'; }
        if(is_year()){ echo '<li>'.get_the_time('Y').'</li>'; }

        echo "</ul>";
}

