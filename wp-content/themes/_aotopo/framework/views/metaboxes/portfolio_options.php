<div class='pyre_metabox'>
<?php
$this->text(	'subtitle',
				'Subtitle',
				'Subtitle will show up below the title on portfolio listing page.'
			);
?>
<?php
$this->text(	'project_link',
				'Project Link',
				'Project link which will show up on website button.'
			);
?>
</div>