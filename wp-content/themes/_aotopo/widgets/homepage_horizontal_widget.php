<?php
add_action('widgets_init', 'pyre_homepage_horizontal_load_widgets');

function pyre_homepage_horizontal_load_widgets()
{
	register_widget('Pyre_Homepage_Horizontal_Widget');
}

class Pyre_Homepage_Horizontal_Widget extends WP_Widget {
	
	function Pyre_Homepage_Horizontal_Widget()
	{
		$widget_ops = array('classname' => 'pyre_homepage_horizontal', 'description' => 'Homepage horizontal recent posts widget.');

		$control_ops = array('id_base' => 'pyre_homepage_horizontal-widget');

		$this->WP_Widget('pyre_homepage_horizontal-widget', 'Crucio: Magazine Horizontal Posts', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		global $post;
		
		extract($args);
		
		$title = $instance['title'];
		$categories = $instance['categories'];
		
		echo $before_widget;

		if($title) {
			echo $before_title.$title.$after_title;
		}
		?>
		<?php
		$recent_posts = new WP_Query(array(
			'cat' => $categories,
			'showposts' => 3
		));
		?>
		<div class="threecolumns">
			<?php while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
			<div class="col">
				<div class="post">
					<?php if(has_post_thumbnail()): ?>
					<div class="img-holder">
						<div class="img-hold">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('horizontal-small'); ?></a>
						</div>
					</div>
					<?php endif; ?>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<div class="meta"><?php the_time('F j Y'); ?>, By <?php the_author_posts_link(); ?>, <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></div>
					<p><?php echo string_limit_words(get_the_excerpt(), 20); ?>...</p>
					<?php if(get_post_meta($post->ID, 'pyre_overall_score', true)): ?>
					<div class="rating">
						<div class="rating-hold" style="width:<?php echo get_post_meta($post->ID, 'pyre_overall_score', true); ?>;"></div>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
		<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['categories'] = $new_instance['categories'];
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Recent Posts', 'categories' => 'all');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('categories'); ?>">Filter by Category:</label> 
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>all categories</option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
	<?php }
}
?>