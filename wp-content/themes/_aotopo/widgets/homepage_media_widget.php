<?php
add_action('widgets_init', 'pyre_homepage_media_load_widgets');

function pyre_homepage_media_load_widgets()
{
	register_widget('Pyre_Homepage_Media_Widget');
}

class Pyre_Homepage_Media_Widget extends WP_Widget {
	
	function Pyre_Homepage_Media_Widget()
	{
		$widget_ops = array('classname' => 'pyre_homepage_media', 'description' => 'Homepage media slider widget.');

		$control_ops = array('id_base' => 'pyre_homepage_media-widget');

		$this->WP_Widget('pyre_homepage_media-widget', 'Cruco: Magazine Carousel', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		global $post;

		extract($args);
		
		$title = $instance['title'];
		$categories = $instance['categories'];
		$posts = $instance['posts'];
		
		echo $before_widget;

		if($title) {
			echo $before_title.$title.$after_title;
		}
		?>
		<div class="music-gallery">
			<div class="music-gallery-holder">
				<ul class="jcarousel-skin-media">
					<?php
					$recent_posts = new WP_Query(array(
						'showposts' => $posts,
						'cat' => $categories,
					));
					?>
					<?php while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
					<?php if(has_post_thumbnail()): ?>
					<li>
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('mag-media'); ?></a>
						<?php if(get_post_meta($post->ID, 'pyre_overall_score', true)): ?>
						<div class="rating">
							<div class="rating-hold" style="width:<?php echo get_post_meta($post->ID, 'pyre_overall_score', true); ?>;"></div>
						</div>
						<?php endif; ?>
						<div class="tooltip">
							<strong><a href="<?php the_permalink(); ?>"><?php echo substr($post->post_title, 0, 15); ?>..</a></strong>
							<span><?php the_time('F j Y'); ?></span>
						</div>
					</li>
					<?php endif; endwhile; ?>
				</ul>
			</div>
		</div>
		<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['categories'] = $new_instance['categories'];
		$instance['posts'] = $new_instance['posts'];
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Media', 'categories' => 'all', 'posts' => 6);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('categories'); ?>">Filter by Category:</label> 
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>all categories</option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('posts'); ?>">Number of posts:</label>
			<input class="widefat" style="width: 30px;" id="<?php echo $this->get_field_id('posts'); ?>" name="<?php echo $this->get_field_name('posts'); ?>" value="<?php echo $instance['posts']; ?>" />
		</p>
	<?php }
}
?>