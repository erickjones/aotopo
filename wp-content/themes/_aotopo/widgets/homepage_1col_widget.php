<?php
add_action('widgets_init', 'pyre_homepage_1col_load_widgets');

function pyre_homepage_1col_load_widgets()
{
	register_widget('Pyre_Homepage_1col_Widget');
}

class Pyre_Homepage_1col_Widget extends WP_Widget {
	
	function Pyre_Homepage_1col_Widget()
	{
		$widget_ops = array('classname' => 'pyre_homepage_1col', 'description' => 'Homepage 1-column recent posts widget.');

		$control_ops = array('id_base' => 'pyre_homepage_1col-widget');

		$this->WP_Widget('pyre_homepage_1col-widget', 'Crucio: Magazine 1-column', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		global $post;
		
		extract($args);
		
		$title = $instance['title'];
		$categories = $instance['categories'];
		
		echo $before_widget;

		if($title) {
			echo $before_title.$title.$after_title;
		}
		?>
		<div class="hold">
			<div class="col">
				<?php
				$recent_posts = new WP_Query(array(
					'cat' => $categories,
					'showposts' => 1
				));
				?>
				<?php while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
				<div class="post">
					<?php
					$args = array(
					    'post_type' => 'attachment',
					    'numberposts' => '5',
					    'post_status' => null,
					    'post_parent' => $post->ID,
						'orderby' => 'menu_order',
						'order' => 'ASC',
					);
					$attachments = get_posts($args);
					if($attachments || has_post_thumbnail() || get_post_meta($post->ID, 'pyre_video', true)):
					?>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						if($('.gallery-item-<?php echo $post->ID; ?> .multiple-images img').length == 1) {
							$('.gallery-item-<?php echo $post->ID; ?> .nav').hide();
						}
						$('.gallery-item-<?php echo $post->ID; ?> .multiple-images').cycle({
							fx: 'fade',
							timeout: 1000,
							slideResize: 0,
							next: '.gallery-item-<?php echo $post->ID; ?> .next',
							prev: '.gallery-item-<?php echo $post->ID; ?> .prev',
						    before: function(c, n, o) {
					        	$(c).css('position', 'absolute');
					        	$(n).css('position', 'relative');
					    	}
						});
					});
					</script>
					<div class="img-holder gallery-item-<?php echo $post->ID; ?>">
						<div class="img-hold multiple-images-container">
							<ul>
								<li class="active">
									<div class="multiple-images">
									<?php if(get_post_meta($post->ID, 'pyre_video', true)): ?>
									<div class="video-wrapper">
										<?php echo get_post_meta($post->ID, 'pyre_video', true); ?>
									</div>
									<?php elseif($attachments): foreach($attachments as $attachment): ?>
										<?php $attachment_image = wp_get_attachment_image_src($attachment->ID, 'two-column-big'); ?>
										<?php $attachment_data = wp_get_attachment_metadata($attachment->ID); ?>
										<a href="<?php the_permalink(); ?>" style="display: block;" class="single-home-image"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment->post_title; ?>" /></a>
									<?php endforeach; else: ?>
										<?php $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'two-column-big'); ?>
										<?php $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id($post->ID)); ?>
										<a href="<?php the_permalink(); ?>" style="display: block;" class="single-home-image"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment->post_title; ?>" /></a>
									<?php endif; ?>
									</div>
									<?php if(!get_post_meta($post->ID, 'pyre_video', true)): ?>
									<div class="nav">
										<a class="prev" href="#">Prev</a>
										<a class="next" href="#">Prev</a>
									</div>
									<?php endif; ?>
								</li>
							</ul>
						</div>
					</div>
					<?php endif; ?>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<div class="meta"><?php the_time('F j Y'); ?>, By <?php the_author_posts_link(); ?>, <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></div>
					<p><?php echo string_limit_words(get_the_excerpt(), 20); ?>...</p>
					<?php if(get_post_meta($post->ID, 'pyre_overall_score', true)): ?>
					<div class="rating">
						<div class="rating-hold" style="width:<?php echo get_post_meta($post->ID, 'pyre_overall_score', true); ?>;"></div>
					</div>
					<?php endif; ?>
				</div>
				<?php endwhile; ?>
			</div>
			<?php
			$recent_posts = new WP_Query(array(
				'showposts' => 3,
				'cat' => $categories,
				'offset' => 1
			));
			if($recent_posts->have_posts()):
			?>
			<div class="col">
				<ul class="main-post-list">
					<?php while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
					<li>
						<?php if(has_post_thumbnail()): ?>
						<span class="img"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('two-column-small'); ?></a></span>
						<?php endif; ?>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<div class="meta"><?php the_time('F j Y'); ?>, By <?php the_author_posts_link(); ?>, <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></div>
					</li>
					<?php endwhile; ?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
		<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['categories'] = $new_instance['categories'];
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Recent Posts', 'categories' => 'all');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('categories'); ?>">Filter by Category:</label> 
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>all categories</option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
	<?php }
}
?>