<?php
add_action('widgets_init', 'social_links_load_widgets');

function social_links_load_widgets()
{
	register_widget('Social_Links_Widget');
}

class Social_Links_Widget extends WP_Widget {
	
	function Social_Links_Widget()
	{
		$widget_ops = array('classname' => 'social_links', 'description' => '');

		$control_ops = array('id_base' => 'social_links-widget');

		$this->WP_Widget('social_links-widget', 'Curcio: Social Links', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);

		?>
		<ul class="social">
			<?php if($instance['rss_link']): ?>
			<li><a class="rss" href="<?php echo $instance['rss_link']; ?>">rss</a></li>
			<?php endif; ?>
			<?php if($instance['fb_link']): ?>
			<li><a class="facebook" href="<?php echo $instance['fb_link']; ?>">facebook</a></li>
			<?php endif; ?>
			<?php if($instance['twitter_link']): ?>
			<li><a class="twitter" href="<?php echo $instance['twitter_link']; ?>">twitter</a></li>
			<?php endif; ?>
			<?php if($instance['dribbble_link']): ?>
			<li><a class="dribble" href="<?php echo $instance['dribbble_link']; ?>">dribbble</a></li>
			<?php endif; ?>
			<?php if($instance['google_link']): ?>
			<li><a class="google" href="<?php echo $instance['google_link']; ?>">google+</a></li>
			<?php endif; ?>
			<?php if($instance['linkedin_link']): ?>
			<li><a class="linkedin" href="<?php echo $instance['linkedin_link']; ?>">linkedin</a></li>
			<?php endif; ?>
			<?php if($instance['blogger_link']): ?>
			<li><a class="blogger" href="<?php echo $instance['blogger_link']; ?>">blogger</a></li>
			<?php endif; ?>
		</ul>
		<?php
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['rss_link'] = $new_instance['rss_link'];
		$instance['fb_link'] = $new_instance['fb_link'];
		$instance['twitter_link'] = $new_instance['twitter_link'];
		$instance['dribbble_link'] = $new_instance['dribbble_link'];
		$instance['google_link'] = $new_instance['google_link'];
		$instance['linkedin_link'] = $new_instance['linkedin_link'];
		$instance['blogger_link'] = $new_instance['blogger_link'];
		return $instance;
	}

	function form($instance)
	{
		$defaults = array();
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('rss_link'); ?>">RSS Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('rss_link'); ?>" name="<?php echo $this->get_field_name('rss_link'); ?>" value="<?php echo $instance['rss_link']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('fb_link'); ?>">Facebook Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('fb_link'); ?>" name="<?php echo $this->get_field_name('fb_link'); ?>" value="<?php echo $instance['fb_link']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('twitter_link'); ?>">Twitter Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('twitter_link'); ?>" name="<?php echo $this->get_field_name('twitter_link'); ?>" value="<?php echo $instance['twitter_link']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('dribbble_link'); ?>">Dribbble Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('dribbble_link'); ?>" name="<?php echo $this->get_field_name('dribbble_link'); ?>" value="<?php echo $instance['dribbble_link']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('google_link'); ?>">Google+ Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('google_link'); ?>" name="<?php echo $this->get_field_name('google_link'); ?>" value="<?php echo $instance['google_link']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('linkedin_link'); ?>">LinkedIn Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('linkedin_link'); ?>" name="<?php echo $this->get_field_name('linkedin_link'); ?>" value="<?php echo $instance['linkedin_link']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('blogger_link'); ?>">Blogger Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('blogger_link'); ?>" name="<?php echo $this->get_field_name('blogger_link'); ?>" value="<?php echo $instance['blogger_link']; ?>" />
		</p>
	<?php
	}
}
?>