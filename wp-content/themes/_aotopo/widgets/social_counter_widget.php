<?php
add_action('widgets_init', 'pyre_social_counter_load_widgets');

function pyre_social_counter_load_widgets()
{
	register_widget('Pyre_Social_Counter_Widget');
}

class Pyre_Social_Counter_Widget extends WP_Widget {
	
	function Pyre_Social_Counter_Widget()
	{
		$widget_ops = array('classname' => 'pyre_social_counter', 'description' => 'Show number of RSS subscribes, twitter followers and facebook fans.');

		$control_ops = array('id_base' => 'pyre_social_counter-widget');

		$this->WP_Widget('pyre_social_counter-widget', 'Crucio: Social Counter', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);

		echo $before_widget;
		?>
		<?php if($instance['twitter_id']): ?>
		<?php
		$interval = 43200;

		if($_SERVER['REQUEST_TIME'] > get_option('pyre_twitter_cache_time')) {
			@$api = wp_remote_get('http://twitter.com/statuses/user_timeline/' . $instance['twitter_id'] . '.json');
			@$json = json_decode($api['body']);
			
			if(@$api['headers']['x-ratelimit-remaining'] >= 1) {
				update_option('pyre_twitter_cache_time', $_SERVER['REQUEST_TIME'] + $interval);
				update_option('pyre_twitter_followers', $json[0]->user->followers_count);
			}
		}
		?>
		<?php endif; ?>

		<?php
		if($instance['feedb_url']) {
			$interval = 43200;
			
			if($_SERVER['REQUEST_TIME'] > get_option('pyre_feedburner_cache_time')) {
				@$api = wp_remote_get('http://feedburner.google.com/api/awareness/1.0/GetFeedData?uri=' . $instance['feedb_url']);
				@$xml = new SimpleXmlElement($api['body'], LIBXML_NOCDATA);
				@$feedburner_followers = (string) $xml->feed->entry['circulation'];
				
				if($feedburner_followers >= 1) {
					update_option('pyre_feedburner_cache_time', $_SERVER['REQUEST_TIME'] + $interval);
					update_option('pyre_feedburner_followers', $feedburner_followers);
				}
			}
		}
		?>

		<div class="social-info">
			<?php
			if($instance['feedb_url']) {
				$rss = $instance['feedb_url'];
				$rss_text = number_format(get_option('pyre_feedburner_followers'));
				$rss_text_2 = __('Subscribers', 'Crucio');
			} else {
				$rss = get_bloginfo('rss2_url');
				$rss_text = __('Subscribe', 'Crucio');
				$rss_text_2 = __('to RSS Feed', 'Crucio');
			}
			?>
			<div class="rss">
				<a href="<?php echo $rss; ?>"><span class="num"><?php echo $rss_text; ?></span></a>
				<a href="<?php echo $rss; ?>"><?php echo $rss_text_2; ?></a>
			</div>
			<?php if($instance['twitter_id']): ?>
			<div class="twitter">
				<a href="http://twitter.com/<?php echo $instance['twitter_id']; ?>"><span class="num"><?php echo number_format(get_option('pyre_twitter_followers')); ?></span></a>
				<a href="http://twitter.com/<?php echo $instance['twitter_id']; ?>"><?php _e('Subscribers', 'Crucio'); ?></a>
			</div>
			<?php endif; ?>
		</div>
		<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['twitter_id'] = $new_instance['twitter_id'];
		$instance['feedb_url'] = $new_instance['feedb_url'];

		return $instance;
	}

	function form($instance)
	{
		$defaults = array('twitter_id' => '', 'feedb_url' => '');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('twitter_id'); ?>">Twitter ID:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('twitter_id'); ?>" name="<?php echo $this->get_field_name('twitter_id'); ?>" value="<?php echo $instance['twitter_id']; ?>" />
		</p>


		<p>
			<label for="<?php echo $this->get_field_id('feedb_url'); ?>">Feedburner URL:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('feedb_url'); ?>" name="<?php echo $this->get_field_name('feedb_url'); ?>" value="<?php echo $instance['feedb_url']; ?>" />
		</p>
	<?php }
}
?>