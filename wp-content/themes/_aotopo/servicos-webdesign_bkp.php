<?php
/*
Template Name: servicos-webdesign
*/
?>

<?php get_header(); ?>
<div class="holder">
	<div class="frame">
		<div class="main-content">

			<div id="content" style="width:100%;">

			<div class="heading">
				<?php if ( function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?>
				<br>
			</div>

			<div class="post" style="padding:0;">	
				<div class="post-content servicos-webdesign">

					<h2>Web Design</h2>
					<p style="margin-top:-20px;">Como você se apresenta no mercado online.</p>
					<p>O serviço de Webdesign envolve algumas etapas. A sua necessidade de negócio vai dizer qual delas adotar e como adota-las. Vamos conhecê-las:</p>

					<div class="servicos-box-header">

						<h2 style="position:relative; top:190px; left:150px; width:250px;"><a href="#servicos-web-estrategia" class="scroll">Estratégia</a></h2>
						<h2 style="position:relative; top:130px; left:400px; width:230px;"><a href="#servicos-web-design" class="scroll">Design</a></h2>
						<h2 style="position:relative; top:110px; left:170px; width:250px;"><a href="#servicos-web-frontend" class="scroll">Front-End</a></h2>
						<h2 style="position:relative; top:59px; left:410px; width:250px;"><a href="#servicos-web-backend" class="scroll">Back-End</a></h2>
						<div class="character-header"></div>
						<div class="bubble" style="width:500px">
							<p class="bubbleText">O que seu negócio precisa?</p>
						</div>
					</div>

					<div id="canvas"></div>

					<div class="servicos-box" id="servicos-web-estrategia">
						<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-estrategia.jpg"?>">
						<div class="caixaTexto">
							<h3>Estratégia</h3>
							<p>Todo projeto começa com o planejamento estratégico. Um brainstorm sobre o que seu negócio necessita para se posicionar no mercado de forma apropriada.

								<br><br>

								De banners a websites, todo projeto deve ser préviamente definido para que você atinja o público desejado estratégicamente. Esse é o momento de traçar metas e reunir análises mercadológicas da sua área de atuação.

								<br><br>

								Tenha em mãos o máximo que puder de dados sobre o mercado atual online ao qual seu negócio atua, e sobre o seu negócio. Todas as idéias surgem aqui.
								<br><br>
								<strong>Nesta etapa podem ser gerados:</strong></p>
								<ul>
									<li>Planejamento</li>
									<li>Briefing</li>
									<li>Wireframes</li>
								</ul>
							</div>
						</div>


						<spam class="separador link-top"><h3>Voltar "aotopo"</h3></spam>


						<div class="servicos-box" id="servicos-web-design">
							<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-design.jpg"?>">
							<div class="caixaTexto">
								<h3>Design</h3>
								<p>Esta etapa é fundamental para a impressão que o seu negócio vai passar. Uma imagem fala mais do que mil palavras e esse é o objetivo do Design. Nós desenhamos pixel a pixel sua reputação online.

									<br><br>

									De lúdico a sóbrio, todo projeto sério precisa de um design customizado para suas necessidades de negócio. Esta etapa é um divisor de águas entre o seu negócio e seus concorrentes. Sua identidade visual é o que distingue você dos demais. Cores, formas e muita criatividade dão vida para seu projeto.

									<br><br>

									No caso de páginas web, o layout deve proporcionar uma leitura direcionada para que sua página não seja apenas um cartão de visitas virtual e passe a fornecer informações válidas para que o usuário interaja e consuma seu produto adequadamente.
									<br><br>
									<strong>Nesta etapa podem ser gerados:</strong></p>
									<ul>
										<li>Identidade visual</li>
										<li>Logos</li>
										<li>Banners</li>
										<li>Animações</li>
										<li>Ilustrações</li>
										<li>Ícones</li>
										<li>Layouts e interfaces</li>
										<li>Campanhas</li>
									</ul>
								</div>
							</div>


							<spam class="separador link-top"><h3>Voltar "aotopo"</h3></spam>


							<div class="servicos-box" id="servicos-web-frontend">
								<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-frontend.jpg"?>">
								<div class="caixaTexto">
									<h3>Desenvolvimento Front-End</h3>
									<p>É nesta etapa que o seu Website, blog ou sistema começa a funcionar na web. O desenvolvimento Front-End é a transformação das imagens geradas pelo designer em páginas funcionais e animadas adequadamente à necessidade do projeto.

										<br><br>

										Aqui são feitas as interações entre o usuário e seu projeto. A estratégia adotada para o projeto faz todo sentido na forma em como suas páginas são navegadas.

										<br><br>

										Toda a estrutura do website, página ou blog é desenvolvida, testada e homologada.

										<br><br>

										O profissional de Front-End geralmente utiliza como combustível, muito café. Um desenvolvedor Front-End criativo é peça fundamental no dinamismo de suas páginas.
										<br><br>
										<strong>Nesta etapa podem ser gerados:</strong></p>
										<ul>
											<li>HTML</li>
											<li>CSS</li>
											<li>JavaScript</li>
											<li>Widgets</li>
											<li>Integrações com redes sociais</li>
											<li>navegação</li>
											<li>Newsletter/EmailMarketing</li>
										</ul>
									</div>
								</div>


								<spam class="separador link-top"><h3>Voltar "aotopo"</h3></spam>


								<div class="servicos-box" id="servicos-web-backend">
									<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-backend.jpg"?>">
									<div class="caixaTexto">
										<h3>Desenvolvimento Back-End</h3>
										<p>Com o Front-End desenvolvido, o Back-End entra em ação e fornece toda a estrutura informacional por trás do seu website. As páginas deixam de fornecer informações "estáticas", ou seja, desenvidas diretamente no html da sua página, e passam a receber estas informações de um banco de dados.

											<br><br>

											O Back-End é responsável por toda a inteligência do seu negócio e transações e interações com outros negócios.

											<br><br>

											O Back-end facilita a renovação das informações da sua página online. Com as tecnologias atuais, você pode facilmente editar e subir informações no seu website de forma independente. Sem a ajuda de um profissional. Com isso você ganha tempo e economiza, tendo um site sempre atualizado e atuante.
											<br><br>
											<strong>Nesta etapa podem ser gerados:</strong></p>
											<ul>
												<li>CMS (Sistema gerenciador de conteúdos online)</li>
												<li>Interações com APIs</li>
												<li>Serviços automatizados</li>
												<li>Cadastros</li>
												<li>Intranets</li>
												<li>Ícones</li>
												<li>Geração de documentos</li>
												<li>Compra e venda online</li>
											</ul>
										</div>
									</div>

									<div class="servicos-webdesign-footer">
										
										<div class="servicos-webdesign-footer-box">
											<h4>Serviços em Web Design</h4>
											<ul class="lista-um">
												<li>Identidade visual</li>
												<li>Logos</li>
												<li>Banners</li>
												<li>Animações</li>
												<li>Ilustrações</li>
												<li>Ícones</li>
											</ul>
											<ul>
												<li>Layouts e interfaces</li>
												<li>Campanhas</li>
												<li>Newsletter/EmailMarketing</li>
												<li>Integrações com redes sociais</li>
												<li>Websites</li>
												<li>Blogs</li>
											</ul>
										</div>

										<div class="servicos-webdesign-footer-tecnologias">
											<h4>Tecnologias utilizadas</h4>
											<ul class="lista-tecnologias">
												<li>HTML / HTML5</li>
												<li>CSS / CSS3</li>
												<li>JavaScript / JQuery</li>
												<li>PHP</li>
												<li>WordPress</li>
											</ul>
										</div>

										<div class="servicos-webdesign-footer-contato">
											<p>O que o seu negócio necessita?
												<br><br>
												<strong>Faça um orçamento!</strong> Ligue para 55 21 9211-4957
												<br>
												ou <a href="#">entre em contato</a>
											</div>
										</div>

										<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>

									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
				<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/servicos-webdesign-v2.js"?>"></script>
				<?php get_footer(); ?>