<?php
// Template Name: Full Width No Title
get_header(); ?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">
							<?php while(have_posts()): the_post(); ?>
							<div id="content" style="width:100%;">
								<div class="post" style="padding:0;">
									<div class="post-content">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>