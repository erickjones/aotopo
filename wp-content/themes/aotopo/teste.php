<?php
    /* Template Name: teste */
    get_header();
?>

				<div class="holder">

					<div id='destaqueHome'>
						<div class="textoDestaqueHome">
							<h3>Aotopo - Webdesign e Marketing Digital</h3>
							<p>Inbound Marketing, Design e Desenvolvimento são as palavras-chave do nosso <strong>negócio</strong>. Histórias, Artigos e Parceria, são aquelas que mais combinam <strong>com</strong> nossa <strong>filosofia</strong>.<br /><br />
			<strong>Conheça a nossa <a href="http://www.aotopo.com.br/experiencia/">experiência</a> e nossos <a href="http://www.aotopo.com.br/servicos/">serviços</a></strong>!</p>
			            </div>
					</div>

					<div class="main-content">

							<div class="content content-home">

								<div id="reguaHome">
									<div id="reguaHome-busca">
										<?php get_search_form(); ?>
									</div>
									<div id="reguaHome-social">
										Conecte-se conosco:
										<ul class="linksSociais">
											<li><a class="linkedin" target="_blank" href="http://www.linkedin.com/company/aotopo">linkedin</a></li>
											<li><a class="gplus" target="_blank" href="https://plus.google.com/116136392359275426287?prsrc=3" rel="publisher">google plus</a></li>
											<li><a class="twitter" target="_blank" href="http://www.twitter.com/aotopo">twitter</a></li>
											<li><a class="rss" target="_blank" href="http://www.aotopo.com.br/wp-rss.php">rss</a></li>
										</ul>
									</div>
								</div>

							<div id="artigosDestaque">
								<?php $maCounter = 0; ?>

								<?php $p = new WP_Query("tag=destaque-2&showposts=4"); ?>
								<?php if($p->have_posts()) : while($p->have_posts() ): $p->the_post(); ?>

								<?php if($maCounter % 2): ?>
									    	<div class="boxArtigoHome boxArtigoHome-par">
									<?php else:?>
											<div class="boxArtigoHome">
									<?php endif; ?>

										<span class="boxArtigoHome-categoria">
										<?php
										$categories = get_the_category();
										$seperator = '  ';
										$output = '';
										if($categories){
											foreach($categories as $category) {
												$output .= '<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "Ver todos os posts em %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
											}
											echo '<span style="color:#ccc;font-size:14px;">//</span> ' . trim($output, $seperator);
										}
										?></span>

										<?php if(has_post_thumbnail( $post_id )): ?>
											<div class="boxArtigoHome-featured-image">
												<a href="<?php the_permalink(); ?>">
													<?php the_post_thumbnail('homeArtigo-img'); ?>
												</a>
											</div>
										<?php endif; ?>

										<h3 class="boxArtigoHome-title"><?php echo the_title(); ?></h3>
										
										<a href="<?php the_permalink(); ?>"><p class="boxArtigoHome-texto"><?php echo limit_words(get_the_excerpt(), '23'); ?>...</p></a>

										<div class="boxArtigoHome-info">
											<span class="boxArtigoHome-autor">Por: <?php the_author_posts_link(); ?></span>
											<span class="boxArtigoHome-comentarios"><?php comments_popup_link('0', '1', '%'); ?></span>
										</div>

									</div>

									<?php $maCounter++ ?>

								<?php endwhile; endif; ?>
								<?php wp_reset_query(); ?>

							</div>

						</div>

						</div>
				</div>

				<div id="servicosHome-box">
					<div id="servicosHome-content">
						<div class="heading headingHome"><h2>Nossos Servicos</h2></div>
						
						<ul id="servicosHome">
							
							<li>
								<a href="#">
									<figure class="servicos-home-imagem">

										<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-webdesign-over.png"?>" alt="Web Design" class="servicos-home-imagem-over">

										<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-webdesign.png"?>" alt="Web Design" class="servicos-home-imagem-out">
										
									</figure>
									<div class="servicos-home-title">Web Design</div>
								</a>
							</li>

							<li>
								<a href="#">
									<figure class="servicos-home-imagem">
									
										<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-inbound-over.png"?>" alt="Web Design" class="servicos-home-imagem-over">

										<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-webdesign.png"?>" alt="Web Design" class="servicos-home-imagem-out">

									</figure>
									<div class="servicos-home-title">Inbound Marketing</div>
								</a>
							</li>

							<li>
								<a href="#">
									<figure class="servicos-home-imagem">

										<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-parcerias-over.png"?>" alt="Web Design" class="servicos-home-imagem-over">

										<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-webdesign.png"?>" alt="Web Design" class="servicos-home-imagem-out">
										
									</figure>
									<div class="servicos-home-title">Parcerias</div>
								</a>
							</li>

						</ul>

					</div>
				</div>

				<div id="reguaHome-social-footer">
					Conecte-se conosco:
					<ul class="linksSociais linksSociais-home">
						<li><a class="linkedin" target="_blank" href="http://www.linkedin.com/company/aotopo">linkedin</a></li>
						<li><a class="gplus" target="_blank" href="https://plus.google.com/116136392359275426287?prsrc=3" rel="publisher">google plus</a></li>
						<li><a class="twitter" target="_blank" href="http://www.twitter.com/aotopo">twitter</a></li>
						<li><a class="rss" target="_blank" href="http://www.aotopo.com.br/wp-rss.php">rss</a></li>
					</ul>
				</div>

				<div id="socialHome">

					<div class="socialHome-box">
						<div class="heading headingHome"><h2>Aotopo no G+</h2></div>
						<div class="g-plus" data-href="https://plus.google.com/116136392359275426287" rel="author"></div>
					</div>

					<div class="socialHome-box">
						<div class="heading headingHome"><h2>Siga-nos no Facebook</h2></div>
						<div class="fb-like-box" data-href="http://www.facebook.com/aotopo.social" data-width="295" data-show-faces="true" data-stream="false" data-border-color="#dcdcdc" data-header="false" style="background-color: #ffffff"></div>
					</div>

					<div class="socialHome-box">
						<div class="heading headingHome"><h2>Últimos Tweets</h2></div>
							<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/jquery.twitter.js"?>"></script>

							<script type="text/javascript">
								jQuery(document).ready(function($) {
									$(".twitter-holder").getTwitter({
										userName: "aotopo",
										numTweets: 3,
										loaderText: "Carregando tweets...",
										slideIn: true,
										slideDuration: 750,
										showHeading: false,
										headingText: "Últimos Tweets",
										showProfileLink: false,
										showTimestamp: true,
										includeRetweets: false,
										excludeReplies: true
									});
								});
							</script>

						<div class="twitter-box">
							<div class="twitter-holder"></div>
						</div>
						<div class="follow">
							<a href="http://twitter.com/<?php echo $twitter_id; ?>"><?php _e('Siga-nos no Twitter....', 'aotopo'); ?></a>
						</div>
					</div>
					
				</div>

				<div id="preFooter">

					<div id="preFooter-content">
					
						<div id="preFooter-cadastro">
							
							<div class="heading headingHome preFooter-title"><h2>Receba novidades mensais</h2></div>
							
							<form id="db_form" action="" method="POST" onsubmit="return validate(this)">

								<input type="text" name="nome" id="nome" value="Nome (obrigatório)" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> class="input-name preFooter-input" />

								<input type="text" name="email" id="email" value="Email (obrigatório)" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> class="input-email preFooter-input"  />

								<div class="button-home button <?php echo of_get_option('skin', 'orange'); ?>"><input name="submit" type="submit" id="submit" tabindex="5" value="Enviar" class="comment-submit preFooter-button" /></div>

								<p id="preFooter-cadastro-log"></p>

							</form>

						</div>

						<div id="preFooter-contato">
							<p><a href="mailto:contato@aotopo.com.br" class="preFooter-link">contato@aotopo.com.br</a><br>
								+55 21 9211-4957
							</p>
						</div>

					</div>


				</div>

				<?php $my_wp_path = get_bloginfo('template_directory'); ?>
				
                <script>
					var wp_path = "<?php echo get_bloginfo('template_directory');?>";
				</script>
                
				<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/home.js"?>"></script>
                
				<div id="fb-root"></div>
				<script>


				
				</script>

<?php get_footer(); ?>