jQuery(document).ready(function($) {

	var myHeight,
	    myTitles = $(".servicos-webdesign-box-header h2"),
	    myChar = $(".character-header"),
	    bubble = $(".bubble"),
	    bubbleText = $(".bubbleText"),
	    animou = false,
	    estrategia = $("#servicos-web-estrategia"),
	    design = $("#servicos-web-design"),
	    frontend = $("#servicos-web-frontend"),
	    backend = $("#servicos-web-backend"),
	    imgEstrategia = estrategia.find("img"),
	    imgDesign = design.find("img"),
	    imgFrontEnd = frontend.find("img"),
	    imgBackend = backend.find("img"),
	    animating = true;

	function init()
	{
		myHeight = $(window).height();
		myChar.css({"opacity" : "0"});
		bubble.css({"opacity" : "0"});

		myTitles.each(function(){
			$(this).css(
				"opacity" , "0"
			);
		});
		resetTitles();
	}
	init();

	$(window).resize(function () {
		init();
	});

	function resetTitles()
	{
		myTitles.eq(0).animate({opacity: "0", top: "180px"}, 200);
		myTitles.eq(1).animate({opacity: "0", top: "120px"}, 200);
		myTitles.eq(2).animate({opacity: "0", top: "100px"}, 200);
		myTitles.eq(3).animate({opacity: "0", top: "49px"}, 200);
	}

	function wordRain()
	{
		resetTitles();

		myTitles.eq(0).delay(0).animate({opacity: "1", top: "190px"}, 300);
		myTitles.eq(1).delay(100).animate({opacity: "1", top: "130px"}, 300);
		myTitles.eq(2).delay(200).animate({opacity: "1", top: "110px"}, 300);
		myTitles.eq(3).delay(300).animate({opacity: "1", top: "59px"}, 300);
		animou = true;
	}

	myTitles.each(function()
	{
		var currentElemTop = $(this).css("top");

		$(this).hover(
			function() {
				$(this).stop().animate({top:'-=10px'}, 200);
			}, 
			function() {
				$(this).stop().animate({top:currentElemTop}, 200);
			}
		);

		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
		});

	});

	
	//disabling animations on media queries event
	function checkingMediaQueries(){

		console.log("checkingMediaQueries on");

		// < 768px
		if ( $(".caixaTexto").css("width") <= "768px") {
			animating = false;
			console.log("animating = false");
		} else if ( $(".caixaTexto").css("width") >= "768px") {
			animating = true;
			console.log("animating = true");
		}
	}

	window.onresize = function(event) {
		checkingMediaQueries();
	}
	

	$(window).scroll(function() {

		var myScrolTop = $('body').scrollTop();

		if(animating === true){
			if(myScrolTop === 0){
			init();
			animou = false;
			}

			if(myScrolTop >= 90){
				
				if(!animou){
					wordRain();
				}
				myChar.stop().animate({top: "-320px", opacity: "1"});
				bubble.stop().animate({top: "-630px", left: "140px", opacity: "1"});

			} else {
				myChar.stop().animate({top: "-280px", opacity:"0"});
				bubble.stop().animate({top: "-590px", left: "200px", opacity: "0"});
				resetTitles();
				animou = false;
			}

			if(myScrolTop > 1040 && myScrolTop <= 1280){
				imgEstrategia.stop().animate({top : "100px"}, 600);
			} else {
				imgEstrategia.stop().animate({top : "0"}, 600);
			}

			if(myScrolTop > 1600 && myScrolTop <= 2029){
				imgDesign.stop().animate({top : "250px"}, 600);
			} else {
				imgDesign.stop().animate({top : "0"}, 600);
			}

			if(myScrolTop > 2370 && myScrolTop <= 2750){
				imgFrontEnd.stop().animate({top : "200px"}, 600);
			} else {
				imgFrontEnd.stop().animate({top : "0"}, 600);
			}

			if(myScrolTop > 3248 && myScrolTop <= 3730){
				imgBackend.stop().animate({top : "200px"}, 600);
			} else {
				imgBackend.stop().animate({top : "0"}, 600);
			}
		}
		
	})
});