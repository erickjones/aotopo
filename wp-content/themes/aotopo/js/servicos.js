jQuery(document).ready(function($) {

	var servicosHomeImagem = $(".servicos-home-imagem");

	servicosHomeImagem.hover(
		function () {
			$(this).find(".servicos-home-imagem-over").stop().fadeIn(300, function(){

			});
			$(this).find(".servicos-home-imagem-out").stop().fadeOut(300, function(){

			});
		},
		function () {
			$(this).find(".servicos-home-imagem-over").fadeOut(300, function(){

			});
			$(this).find(".servicos-home-imagem-out").fadeIn(300, function(){

			});
		}
		);

});