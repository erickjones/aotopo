jQuery(document).ready(function($) {

	var myTitles = $(".linkNavegacao");

	myTitles.each(function()
	{
		var currentElemTop = $(this).css("top");

		$(this).hover(
			function() {
				$(this).stop().animate({top:'-=10px'}, 200);
			}, 
			function() {
				$(this).stop().animate({top:currentElemTop}, 200);
			}
		);

		$(this).click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
		});

	});


});