var interval;

function validate(obj)
{

	// Forget yourself not of JS validation !

	var campoNome = document.getElementById("nome");
	if(campoNome.value == "" || campoNome.value == "Nome (obrigatório)")
	{
		document.getElementById("preFooter-cadastro-log").innerHTML = "Preencha o campo Nome corretamente";
		campoNome.focus();
		return false;
	}

	var campoEmail = document.getElementById("email");

	if(campoEmail.value == "" || campoEmail.value == "Email (obrigatório)" || campoEmail.value.indexOf('@')==-1 || campoEmail.value.indexOf('.')==-1)
	{
		document.getElementById("preFooter-cadastro-log").innerHTML = "Preencha o campo Email corretamente";
		campoEmail.focus();
		return false;
	}
			
	var data = "nome="+campoNome.value+"&email=" + campoEmail.value;

	xmlhttp.open("POST", wp_path + "/ajax_call.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(data);

	return false;
	
}

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
	var xmlhttp = new XMLHttpRequest();
}
else
{// code for IE6, IE5
	var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
}

xmlhttp.onreadystatechange=function()
{
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
		document.getElementById("preFooter-cadastro-log").innerHTML = xmlhttp.responseText;
	}
	
	interval = window.setTimeout('clear()', 2000);
}


function clear()
{
	if (document.getElementById("preFooter-cadastro-log").innerHTML.indexOf("sucesso") > 0)
	{
		document.getElementById("nome").value = document.getElementById("email").value = "";
	}
	
	document.getElementById("preFooter-cadastro-log").innerHTML = "";
	clearTimeout(interval);
	
}


jQuery(document).ready(function($) {

	var boxArtigoHome = $(".boxArtigoHome");
	var servicosHomeImagem = $(".servicos-home-imagem");

	boxArtigoHome.hover(
		function () {
			$(this).find(".boxArtigoHome-texto").stop().fadeIn(300, function(){

			});
			$(this).find(".boxArtigoHome-title").stop().fadeOut(300, function(){

			});
		},
		function () {
			$(this).find(".boxArtigoHome-texto").fadeOut(300, function(){

			});
			$(this).find(".boxArtigoHome-title").fadeIn(300, function(){

			});
		}
		);


	servicosHomeImagem.hover(
		function () {
			$(this).find(".servicos-home-imagem-over").stop().fadeIn(300, function(){

			});
			$(this).find(".servicos-home-imagem-out").stop().fadeOut(300, function(){

			});
		},
		function () {
			$(this).find(".servicos-home-imagem-over").fadeOut(300, function(){

			});
			$(this).find(".servicos-home-imagem-out").fadeIn(300, function(){

			});
		}
		);

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=240294509359186";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

});





