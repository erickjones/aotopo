<?php
add_action('widgets_init', 'archives_categories_load_widgets');

function archives_categories_load_widgets()
{
	register_widget('Archives_Categories_Widget');
}

class Archives_Categories_Widget extends WP_Widget {
	
	function Archives_Categories_Widget()
	{
		$widget_ops = array('classname' => 'archives_categories', 'description' => '.');

		$control_ops = array('id_base' => 'archives_categories-widget');

		$this->WP_Widget('archives_categories-widget', 'Curcio: Archives/Categories', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);

		echo $before_widget;
		?>
		<div class="col">
			<div class="heading">
				<h3><?php _e('Archives', 'Crucio'); ?></h3>
			</div>
			<ul>
				<?php wp_get_archives(); ?>
			</ul>
		</div>
		<div class="col">
			<div class="heading">
				<h3><?php _e('Categories', 'Crucio'); ?></h3>
			</div>
			<ul>
				<?php wp_list_categories('title_li='); ?>
			</ul>
		</div>
	</div>
	<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		return $instance;
	}

	function form($instance)
	{}
}
?>