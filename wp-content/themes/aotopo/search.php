<?php get_header(); ?>
<div class="holder">
	<div class="frame">
		<div class="main-content">
			<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">
				<div class="heading">
					<h2>
						<?php printf(__('Search results for %s', 'Crucio'), get_search_query()); ?>
					</h2>
				</div>
				<?php while(have_posts()): the_post(); ?>
				<div class="post posts-archive" style="padding: 0;">
					<?php if(has_post_thumbnail()): ?>
					<div class="featured-image">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('archive-img'); ?></a>
					</div>
				<?php endif; ?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

				<div class="meta">
					<span class="data"><?php the_time('j \d\e F'); ?></span>
					<span class="tags">
						<?php
						$categories = get_the_category();
						$seperator = '  ';
						$output = '';
						if($categories){
							foreach($categories as $category) {
								$output .= '<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.', ';
							}
							echo trim($output, $seperator);
						}
						?></span>
						<span class="comentarios"><?php comments_popup_link('Sem comentários', '1 Comentário', '% Comentários'); ?></span>
					</div>
					
					<div class="post-content">
						<p><?php echo string_limit_words(get_the_excerpt(), 20); ?> ...</p>
						<?php if(of_get_option('read_more', 'yes') == 'yes'): ?>
						<div class='button <?php echo of_get_option('skin', 'orange'); ?> read-more'><a href="<?php the_permalink(); ?>"><?php _e('Leia mais', 'Crucio'); ?></a></div>
					<?php endif; ?>
				</div>
			</div>
		<?php endwhile; ?>
		<?php kriesi_pagination($pages = '', $range = 2); ?>
	</div>
	<?php get_sidebar(); ?>
</div>
</div>
</div>
<?php get_footer(); ?>