
<?php get_header(); ?>
<div class="holder">
	<div class="frame">
		<div class="main-content">

			<div class="heading"><?php if ( function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?></div>

			<?php while(have_posts()): the_post(); ?>
			<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">
				<div id="post-<?php the_ID(); ?>" <?php post_class('post single-post'); ?> style="padding: 0;">
					<?php
					$args = array(
						'post_type' => 'attachment',
						'numberposts' => '5',
						'post_status' => null,
						'post_parent' => $post->ID,
						'orderby' => 'menu_order',
						'order' => 'ASC',
						);
					$attachments = get_posts($args);
					if($attachments || has_post_thumbnail() || get_post_meta($post->ID, 'pyre_video', true)):
						?>
					
				<?php endif; ?>

				<div class="post-content-container">
					<div id="topoSingle">
						<h1><?php the_title(); ?></h1>
						<span class="autor">Por: <?php the_author_posts_link(); ?></span>
					</div>	
					<div class="meta">
						<span class="data"><?php the_time('j \d\e F'); ?></span>
						<span class="tags"><?php the_category(', '); ?></span>
						<span class="comentarios"><?php comments_popup_link('Sem comentários', '1 Comentário', '% Comentários'); ?></span>
					</div>


					<div class="post-content">
						<?php the_content(); ?>
						<?php wp_link_pages(); ?>
						<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>
						<?php if ( function_exists( 'authorbbio_add_authorbox' ) ) authorbbio_add_authorbox(); ?>
					</div>

				</div>
				<?php if(of_get_option('comments') == 'enable'): ?>
				<?php wp_reset_query(); ?>
			<?php endif; ?>
			<?php comments_template(); ?>
		</div>
		<div class="single-navigation clearfix">
			<div class="alignleft"><?php previous_post_link('%link', __('<span></span>Anterior', 'Broadway')); ?></div>
			<div class="alignright"><?php next_post_link('%link', __('Próximo<span></span>', 'Broadway')); ?></div>
		</div>
	</div>
<?php endwhile; ?>
<?php get_sidebar(); ?>
</div>
</div>
</div>
<?php get_footer(); ?>