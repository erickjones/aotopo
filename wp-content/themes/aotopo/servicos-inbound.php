<?php
/*
Template Name: servicos-inbound
*/
?>

<?php get_header(); ?>
<div class="holder">
	<div class="frame">
		<div class="main-content">

			<div id="content" style="width:100%;">

			<div class="heading">
				<?php if ( function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?>
				<br>
			</div>

			<div class="post" style="padding:0;">	
				<div class="post-content servicos-interna">

					<h2>Inbound Marketing</h2>
					
					<div id="imagemPage"><img src="<?php echo get_bloginfo('template_directory')."/images/servicos-inbound.png"?>" alt="Inbound Marketing"></div>

					<div class="textIntro">
						<p>O Inbound Marketing é baseado no conceito de ganhar a atenção de visitantes, produzindo conteúdo de valor e tornando-se fácil de ser encontrado e trazer clientes para seu site.</p>
						<p>É como <strong>cultivar seu jardim</strong>. </p>
						<p>Faça seu <a title="Orçamento Grátis" href="http://www.aotopo.com.br/contato/" target="_blank">orçamento já</a> ou saiba mais sobre nossos serviços:<br><br><a href="#servicos-inbound-intelligence" class="linkNavegacao">Análise e Planejamento</a>, <a href="#servicos-inbound-conteudo" class="linkNavegacao">Marketing de Conteúdo</a>, <a href="#servicos-inbound-busca" class="linkNavegacao">Marketing de Busca</a>, <a href="#servicos-inbound-midiasSociais" class="linkNavegacao">Mídias Sociais</a>, <a href="#servicos-inbound-patrocinados" class="linkNavegacao">Publicidade</a> e <a href="#servicos-inbound-analytics" class="linkNavegacao">Google Analytics</a>.
					</div>

					<div id="canvas"></div>

					<div class="servicos-box" id="servicos-inbound-intelligence">
						<img src="<?php echo get_bloginfo('template_directory')."/images/inbound-planejamento.jpg"?>">
						<div class="caixaTexto">
							<h3>Análise e Planejamento</h3>
							<p>Nosso serviço começa sempre por uma <strong>análise profunda</strong> do cenário atual do seu site e um <strong>planejamento estratégico</strong>.

								<br><br>
							<strong>A diferentes etapas dessa consultoria são:</strong></p>
							<ul>
								<li>Briefing</li>
								<li>Análise de mercado</li>
								<li>Análise de dados e códigos</li>
								<li>Elaboração de estratégia digital</li>
							</ul>
						</div>
					</div>


						<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


								<div class="servicos-box" id="servicos-inbound-conteudo">
									<img src="<?php echo get_bloginfo('template_directory')."/images/inbound_webwritting.jpg"?>">
									<div class="caixaTexto">
										<h3>Marketing de Conteúdo</h3>
										<p>Quem nunca leu ou ouviu a seguinte frase "O Conteúdo é rei"?  Nada mais que uma afirmação dita por Bill Gates, fundador da Microsoft.   <strong>O conteúdo é o eixo principal do seu site</strong>. Um site não existe por si só...
											<br><br>
											<strong>Para ajudar você com o conteúdo do seu site, podemos:</strong></p>
											<ul>
												<li>Criar seu marketing digital e suas campanhas</li>
												<li>Redação web para matérias, produtos, etc.</li>
												<li>Criação de conteúdo específico para landing pages</li>
												<li>Criação de Infográficos</li>
												<li>Traduções (Inglês, Espanhol, Francês)</li>
												<li>Cursos de redação para web (Webwriting)</li>
											</ul>
										</div>
									</div>

									<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


						<div class="servicos-box" id="servicos-inbound-busca">
							<img src="<?php echo get_bloginfo('template_directory')."/images/inbound_seo.jpg"?>">
							<div class="caixaTexto">
								<h3>Marketing de Busca ou SEO</h3>
								<p>O SEO ou Otimização para Motores de Busca, é <strong>a arte de indexar seu site para aumentar sua visibilidade</strong> na web.
									<br><br>
									<strong>As diferentes etapas desse processo são:</strong></p>
									<ul>
										<li>Otimização de código e Web Semântica (Front-End)</li>
										<li>Otimização de Conteúdo (Webwriting)</li>
										<li>Relações Públicas Online (Link Building)</li>
										<li>Otimização de Mídias Sociais (Social Media Optimization)</li>
										<li>Gerenciamento da ferramenta Google Webmaster Tools</li>
									</ul>
								</div>
							</div>


							<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


							<div class="servicos-box" id="servicos-inbound-midiasSociais">
								<img src="<?php echo get_bloginfo('template_directory')."/images/inbound_sociais.jpg"?>">
								<div class="caixaTexto">
									<h3>Mídias Sociais</h3>
									<p>A Mídias Sociais são o canal direto com seu cliente, uma revolução digital. <br />Para <strong>gerenciar sua comunidade e a reputação online da sua empresa</strong>, você precisa de um analista de redes sociais (community manager) que irá criar e compartilhar conteúdo relevante, criando assim um laço afetivo entre sua marca e seus consumidores.
										<br><br>
										<strong>Atuamos nas principais redes sociais:</strong></p>
										<ul>
											<li>Facebook</li>
											<li>Linkedin</li>
											<li>Twitter</li>
											<li>Google+</li>
										</ul>
									</div>
								</div>


								<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


								<div class="servicos-box" id="servicos-inbound-patrocinados">
									<img src="<?php echo get_bloginfo('template_directory')."/images/inbound_links.jpg"?>">
									<div class="caixaTexto">
										<h3>Publicidade</h3>
										<p>Os Links Patrocinados são <strong>uma ferramenta poderosa</strong> dentro de uma estratégia. Os anúncios e as peças publicitárias são sempre recursos muito úteis no lançamento de uma campanha. Quanto melhor utilizado, menos SPAM.<br /><br />
										Hoje, é possível otimizar suas campanhas de publicidade em termos de custo-benefício.
										<br><br>
											<strong>Atuamos com foco em performance:</strong></p>
											<ul>
												<li>Criação de anúncios</li>
												<li>Criação de campanhas</li>
												<li>Google Adwords</li>
												<li>Facebook Ads</li>
												<li>Linkedin Ads</li>
												<li>Youtube</li>
											</ul>
										</div>
									</div>

									<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


								<div class="servicos-box" id="servicos-inbound-analytics">
									<img src="<?php echo get_bloginfo('template_directory')."/images/inbound_analytics.jpg"?>">
									<div class="caixaTexto">
										<h3>Google Analytics</h3>
										<p>O Google Analytics, ou qualquer ferramenta de Web Analytics, é um elemento incontornável de qualquer atuação na Internet. Navegue sem dados, é você irá encalhar. <br><br>
										O monitoramento e a interpretação correta de dados necessita um olho experimentado. 
										Nosso papel é de <strong>acompanhar você na sua tomada de decisões</strong>.<br><br>
											<strong>Os nossos serviços nesse sentido são:</strong></p>
											<ul>
												<li>Inserção de código (tagueamento)</li>
												<li>Configuração do Google Analytics</li>
												<li>Monitoramento de campanhas</li>
												<li>Análise de dados e resultados</li>
												<li>Criação de relatórios</li>
												<li>Business Intelligence</li>
											</ul>
										</div>
									</div>

									<div class="servicos-interna-footer">
										
										<h4>Serviços</h4>
										<ul class="servicos-interna-footer-lista">
											<li>Análise e Planejamento</li>
											<li>Marketing de Conteúdo</li>
											<li>Marketing de Busca</li>
											<li>Mídias Sociais</li>
											<li>Publicidade</li>
											<li>Google Analytics</li>
										</ul>
										<ul class="servicos-interna-footer-lista">
											<li>Business Intelligence</li>
											<li>Webwriting</li>
											<li>Search Engine Optimization</li>
											<li>Social Media</li>
											<li>Links Patrocinados</li>
											<li>Web Analytics</li>
										</ul>

										<div class="servicos-interna-footer-lista lastList">
											<h4>Ferramentas</h4>
											<ul class="lista-tecnologias">
												<li>Google Analytics</li>
												<li>Content Management System</li>
												<li>Google Webmaster Tools</li>
												<li>Hootsuite</li>
												<li>Google Adwords</li>
												<li>SEO PowerSuite</li>
											</ul>
										</div>

										<div class="servicos-interna-footer-contato">
											<p>O que o seu negócio necessita?
												<br><br>
												<strong>Faça um orçamento!</strong> Ligue para 55 21 9211-4957
												<br>
												ou <a href="/contato/">entre em contato</a>
											</div>
										</div>

										<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>

									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
				<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/servicos-interna.js"?>"></script>
				<?php get_footer(); ?>