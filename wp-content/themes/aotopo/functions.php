<?php
add_theme_support('automatic-feed-links');

// Register Navigation
register_nav_menu('top_navigation', 'Top Navigation');
register_nav_menu('main_navigation', 'Main Navigation');

// Defaults
$content_width = 960;

// Excerpt
function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Translation
load_theme_textdomain('Crucio', TEMPLATEPATH.'/languages');
$locale = get_locale();
$locale_file = TEMPLATEPATH."/languages/$locale.php";
if (is_readable($locale_file)) {
	require_once($locale_file);
}

// Register widgetized locations
if(function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="heading"><h3>',
		'after_title' => '</h3></div>',
	));

	register_sidebar(array(
		'name' => 'Magazine',
		'before_widget' => '<div id="%1$s" class="section %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="heading"><h2>',
		'after_title' => '</h2></div>',
	));

	register_sidebar(array(
		'name' => 'Footer',
		'before_widget' => '<div id="%1$s" class="col">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h4>',
		'after_title' => '</h4></div>',
	));
}

// Include widgets
include_once('widgets/widgets.php');

// Metaboxes
include_once('framework/metaboxes.php');

// Custom Functions
include_once('framework/custom_functions.php');

// Shortcodes
include_once('shortcodes.php');

// Add post thumbnail functionality
add_theme_support('post-thumbnails');
add_image_size('two-column-big', 340, 189, true);
add_image_size('two-column-small', 52, 48, true);
add_image_size('horizontal-small', 334, 235, true);
add_image_size('mag-media', 145, 95, true);
add_image_size('tabs-img', 45, 45, true);
add_image_size('single-img', 592, 274, true);
add_image_size('archive-img', 275, 152, true);
add_image_size('gallery-img', 342, 208, true);
add_image_size('homeArtigo-img', 80, 80, true);

// Register custom post types
add_action('init', 'pyre_init');
function pyre_init() {
	register_post_type(
		'crucio_portfolio',
		array(
			'labels' => array(
				'name' => 'Portfolio',
				'singular_name' => 'Portfolio'
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'portfolio-items'),
			'supports' => array('title', 'editor', 'thumbnail','comments'),
			'can_export' => true,
		)
	);

	register_taxonomy('portfolio_category', 'crucio_portfolio', array('hierarchical' => true, 'label' => 'Categories', 'query_var' => true, 'rewrite' => true));

	register_post_type(
		'crucio_gallery',
		array(
			'labels' => array(
				'name' => 'Gallery',
				'singular_name' => 'Gallery'
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'gallery-items'),
			'supports' => array('title', 'editor', 'thumbnail','comments'),
			'can_export' => true,
		)
	);
}

if ( !function_exists( 'optionsframework_init' ) ) {

	/*-----------------------------------------------------------------------------------*/
	/* Options Framework Theme
	/*-----------------------------------------------------------------------------------*/

	/* Set the file path based on whether the Options Framework Theme is a parent theme or child theme */

	if ( STYLESHEETPATH == TEMPLATEPATH ) {
		define('OPTIONS_FRAMEWORK_URL', TEMPLATEPATH . '/admin/');
		define('OPTIONS_FRAMEWORK_DIRECTORY', get_bloginfo('template_directory') . '/admin/');
	} else {
		define('OPTIONS_FRAMEWORK_URL', STYLESHEETPATH . '/admin/');
		define('OPTIONS_FRAMEWORK_DIRECTORY', get_bloginfo('stylesheet_directory') . '/admin/');
	}

	require_once (OPTIONS_FRAMEWORK_URL . 'options-framework.php');

}

/* 
 * This is an example of how to add custom scripts to the options panel.
 * This example shows/hides an option when a checkbox is clicked.
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>
<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#example_showhidden').click(function() {
  		jQuery('#section-example_text_hidden').fadeToggle(400);
	});
	
	if (jQuery('#example_showhidden:checked').val() !== undefined) {
		jQuery('#section-example_text_hidden').show();
	}
	
});
</script>
<?php
}




// How comments are displayed
function crucio_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<?php $add_below = ''; ?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
	
		<div class="the-comment">
			<div class="avatar">
				<?php echo get_avatar($comment, 54); ?>
			</div>
			
			<div class="comment-box">
			
				<div class="comment-author meta">
					<strong><?php echo get_comment_author_link() ?></strong>
					<?php printf(__('%1$s at %2$s', 'Crucio'), get_comment_date(),  get_comment_time()); echo ' - ' ?></a><?php edit_comment_link(__('Editar'),'  ',' - '); ?><?php comment_reply_link(array_merge( $args, array('reply_text' => 'Responder', 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</div>
			
				<div class="comment-text">
					<?php if ($comment->comment_approved == '0') : ?>
					<em><?php _e('Seu comentário está aguardando moderação.', 'Crucio') ?></em>
					<br />
					<?php endif; ?>
					<?php comment_text() ?>
				</div>
			
			</div>
			
		</div>
<?php }


function limit_words($string, $word_limit) {
 
	// creates an array of words from $string (this will be our excerpt)
	// explode divides the excerpt up by using a space character
 
	$words = explode(' ', $string);
 
	// this next bit chops the $words array and sticks it back together
	// starting at the first word '0' and ending at the $word_limit
	// the $word_limit which is passed in the function will be the number
	// of words we want to use
	// implode glues the chopped up array back together using a space character
 
	return implode(' ', array_slice($words, 0, $word_limit));
 
}