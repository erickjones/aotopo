<?php
// Template Name: Magazine
get_header(); ?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">
							<?php if(of_get_option('slider_type') == 'full-width'): ?>
								<?php include_once('full-width-slider.php'); ?>
							<?php endif; ?>
							<?php if(of_get_option('slider_type') == 'full-width-text'): ?>
								<?php include_once('full-width-text-slider.php'); ?>
							<?php endif; ?>
							<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">
								<?php if(of_get_option('slider_type') == 'half-width'): ?>
									<?php include_once('half-width-slider.php'); ?>
								<?php endif; ?>
								<?php if(of_get_option('slider_type') == 'half-width-text'): ?>
									<?php include_once('half-width-text-slider.php'); ?>
								<?php endif; ?>
								<?php
								if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Magazine')): 
								endif;
								?>
							</div>
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>