<?php
/*
Template Name: servicos-webdesign
*/
?>

<?php get_header(); ?>
<div class="holder">
	<div class="frame">
		<div class="main-content">

			<div id="content" style="width:100%;">

			<div class="heading">
				<?php if ( function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?>
				<br>
			</div>

			<div class="post" style="padding:0;">	
				<div class="post-content servicos-interna">

					<h2>Web Design</h2>
					<div id="imagemPage"><img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign.png"?>" alt="Web Design"></div>
					<div class="textIntro">
						<p>A <strong>criação de site de qualidade</strong> envolve alguns passos. <br>A necessidade do seu negócio dirá qual delas adotar e como.
						<p>Faça seu <a title="Orçamento Grátis" href="http://www.aotopo.com.br/contato/" target="_blank">orçamento já</a> ou saiba mais sobre nossos serviços de Web Design:<br><br>
					<a href="#servicos-web-estrategia" class="linkNavegacao">Arquitetura da Informação</a><br>
					<a href="#servicos-web-design" class="linkNavegacao">Design</a><br>
					<a href="#servicos-web-frontend" class="linkNavegacao">Desenvolvimento Front-End</a><br>
					<a href="#servicos-web-backend" class="linkNavegacao">Desenvolvimento Back-End</a><br>
					</div>

					<div class="servicos-box" id="servicos-web-estrategia">
						<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-ai.jpg"?>">
						<div class="caixaTexto">
							<h3>Arquitetura da Informação</h3>
							<p>Arquitetura da Informação (AI) é a arte e ciência de organização e rotulação de sites web.<br><br>É um estudo prévio, respeitando as boas práticas, que visa a <strong>pensar e melhorar a estrutura do seu site</strong>, isso antes da construção. Uma ótima forma de evitar retrabalhos e assim otimizar seu investimento.
							<br><br>
								<strong>Serviços:</strong></p>
								<ul>
									<li>Pesquisa de concorrência</li>
									<li>Esboços</li>
									<li>Wireframes</li>
									<li>Storyboards</li>
								</ul>
							</div>
						</div>


						<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


						<div class="servicos-box" id="servicos-web-design">
							<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-design.jpg"?>">
							<div class="caixaTexto">
								<h3>Design</h3>
								<p>Quem nunca leu ou ouviu a seguinte frase: "uma imagem vale mais que mil palavras". O Design é fundamental.<br><br>Sua reputação online precisa ser customizada para seduzir seu público-alvo, isso respeitando os padrões do seu mercado. Desenhamos pixel a pixel <strong>sua marca e identidade visual</strong>.
									<br><br>
									<strong>Serviços:</strong></p>
									<ul>
										<li>Logo</li>
										<li>Layout (interface)</li>
										<li>Ilustração</li>
										<li>Infográfico</li>
										<li>Banner</li>
										<li>Ícone</li>
										<li>Animação</li>
									</ul>
								</div>
							</div>


							<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


							<div class="servicos-box" id="servicos-web-frontend">
								<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-frontend.jpg"?>">
								<div class="caixaTexto">
									<h3>Desenvolvimento Front-End</h3>
									<p>O processo de <strong>montar o site</strong>.<br>O Front-End, ou desenvolvimento de interface, é <strong>a mágica que faz funcionar seu site</strong> na web.<br><br>
										Toda a estrutura do seu website precisa ser desenvolvida, testada e homologada para atingir resultados otimizados. <br><br>Sabemos criar, organizar e gerenciar um site com talento.<br><br>
										<strong>Tecnologias:</strong></p>
										<ul>
											<li>HTML</li>
											<li>CSS</li>
											<li>JavaScript</li>
											<li>Widgets</li>
											<li>Integração de APIs</li>
											<li>Usabilidade</li>
											<li>Newsletter/Email Marketing</li>
										</ul>
									</div>
								</div>


								<spam class="separador link-top"><h3>Voltar "Aotopo"</h3></spam>


								<div class="servicos-box" id="servicos-web-backend">
									<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-webdesign-backend.jpg"?>">
									<div class="caixaTexto">
										<h3>Desenvolvimento Back-End</h3>
										<p>O desenvolvimento da <strong>estrutura informacional por trás do seu website</strong>. As páginas passam a receber informações "dinâmicas", de um <strong>banco de dados</strong>.
											<br><br>
											O Back-End é responsável por toda a inteligência do seu negócio, transações e interações com outros negócios.
											<br><br>
											O uso de sistemas gerenciadores de conteúdo (CMS - Content Management System) facilita a atualização do conteúdo das suas páginas de forma independente. 							<br><br>
											<strong>Sistema e funcionalidades:</strong></p>
											<ul>
												<li>CMS (Sistema Gerenciador de Conteúdo)</li>
												<li>Integrações com APIs</li>
												<li>Cadastros</li>
												<li>Intranets</li>
												<li>Geração de documentos</li>
												<li>Comércio eletrônico (E-Commerce)</li>
											</ul>
										</div>
									</div>

									<div class="servicos-interna-footer">
										
										<h4>Serviços e Produtos</h4>
										<ul class="servicos-interna-footer-lista">
											<li>Arquitetura</li>
											<li>Design</li>
											<li>Interface</li>
											<li>Banco de dados</li>
										</ul>
										<ul class="servicos-interna-footer-lista">
											<li>Website</li>
											<li>Hotsite</li>
											<li>Multimídia</li>
											<li>Peças publicitárias</li>
										</ul>
										<div class="servicos-interna-footer-lista lastList">
											<h4>Tecnologias</h4>
											<ul class="lista-tecnologias">
												<li>HTML / HTML5</li>
												<li>CSS / CSS3</li>
												<li>JavaScript / JQuery</li>
												<li>PHP</li>
											</ul>
										</div>

										<div class="servicos-interna-footer-contato">
											<p>O que o seu negócio necessita?
												<br><br>
												<strong>Faça um orçamento!</strong> Ligue para 55 21 9211-4957
												<br>
												ou <a href="/contato/">entre em contato</a>
											</div>
										</div>

										<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>

									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
				<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/servicos-interna.js"?>"></script>
				<?php get_footer(); ?>