<div class="pyre_metabox">
<?php
$this->text(	'email',
				'Email Address'
			);
?>
<?php
$this->text(	'address',
				'Address'
			);
?>
<?php
$this->text(	'phone',
				'Phone'
			);
?>
<?php
$this->textarea(	'gmap',
				'Google Maps Embed Code (592x274px)'
			);
?>
</div>