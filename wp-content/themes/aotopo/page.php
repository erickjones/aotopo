<?php get_header(); ?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">

							<div class="heading">
								<?php if (function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?>
								<br>
							</div>

							
							
							<?php while(have_posts()): the_post(); ?>
							<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">
								<h3 class="fixPageTitle"><?php the_title(); ?></h3>
								<div class="post" style="padding:0;">
									<div class="post-content">
										<?php the_content(); ?>
										<?php wp_link_pages(); ?>
									</div>
								</div>
							</div>
							<?php endwhile; ?>
							<?php get_sidebar(); ?>
							<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>