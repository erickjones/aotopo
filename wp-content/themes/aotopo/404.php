<?php
get_header(); ?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">

							<div class="heading">
								<?php if (function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?>
								<br>
							</div>

							<h3 class="fixPageTitle"><?php the_title(); ?></h3>

							<div id="content" style="<?php if(of_get_option('sidebar_position', 'right') == 'left') { echo 'float:right;'; } ?>">

							<div id="content" style="width:100%;">
								<div class="404" style="padding:0;">
									<div class="heading"><h2><?php _e('Ops...', 'Crucio'); ?> <span><?php _e('A página que você está procurando não existe.', 'Crucio'); ?></span></h2></div>
									<div class="404-content">
										<div class="search-404">
											<p><strong><?php _e('Faça uma busca', 'Crucio'); ?></strong></p>
											<p><span><?php _e('Digite abaixo o que você procura:', 'Crucio'); ?></span></p>
											<?php get_search_form(); ?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php get_sidebar(); ?>

					</div>
				</div>
				<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/404.js"?>"></script>
<?php get_footer(); ?>