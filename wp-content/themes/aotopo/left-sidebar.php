<?php
// Template Name: Left Sidebar
get_header(); ?>
				<div class="holder">
					<div class="frame">
						<div class="main-content">
							<?php while(have_posts()): the_post(); ?>
							<div id="content" style="float:right;">
								<div class="post" style="padding:0;">
									<div class="heading"><h2><?php the_title(); ?></h2></div>
									<div class="post-content">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
							<?php endwhile; ?>
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>