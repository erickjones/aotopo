<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" />
	<?php if (is_home()) { ?>
		<meta property="og:image" content="<?php echo get_bloginfo('template_directory')."/images/logo_bolota.png"?>"/>
	<?php } ?>

	<title><?php bloginfo('name'); ?> <?php wp_title(' - ', true, 'left'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="stylesheet" href="" name="css-skin" />

	<meta name="author" content="Aotopo | Inbound Marketing e Webdesign" href="https://plus.google.com/116136392359275426287/">

	<link href='http://fonts.googleapis.com/css?family=<?php echo urlencode(of_get_option('google_font', 'PT Sans')); ?>:400,400italic,700,700italic' rel='stylesheet' type='text/css' />

	<?php wp_enqueue_script('jquery'); ?>
	<?php
	if(
		is_front_page()|| 
		is_home() || 
		is_page_template('magazine.php') ||
		is_page_template('mag-full-width-text-slider.php') ||
		is_page_template('mag-full-width-thumbnails-slider.php') ||
		is_page_template('mag-half-width-text-slider.php') ||
		is_page_template('mag-half-width-thumbnails-slider.php')
	):
	?>
	<?php wp_enqueue_script('jquery.flexslider', get_bloginfo('template_directory').'/js/jquery.flexslider.js'); ?>
	<?php endif; ?>
	<?php wp_enqueue_script('jCarousel', get_bloginfo('template_directory'). '/js/jquery.jcarousel.min.js'); ?>
	<?php wp_enqueue_script('jtwt', get_bloginfo('template_directory'). '/js/jtwt.js'); ?>
	<?php wp_enqueue_script('jquery.cycle', get_bloginfo('template_directory').'/js/jquery.cycle.all.js'); ?>
	<?php wp_enqueue_script('jquery.prettyPhoto', get_bloginfo('template_directory').'/js/jquery.prettyPhoto.js'); ?>
	<?php wp_enqueue_script('jquery.fitvids', get_bloginfo('template_directory').'/js/jquery.fitvids.js'); ?>
	<?php wp_enqueue_script('crucio', get_bloginfo('template_directory').'/js/main.js'); ?>
	<?php wp_enqueue_script('css3-mediaqueries', get_bloginfo('template_directory').'/js/css3-mediaqueries.js'); ?>
	<?php wp_head(); ?>
	
	<script type="text/javascript">
	jQuery(window).load(function() {
		<?php
		if(
			(is_front_page()|| 
			is_home() || 
			is_page_template('magazine.php') ||
			is_page_template('mag-full-width-text-slider.php') ||
			is_page_template('mag-full-width-thumbnails-slider.php') ||
			is_page_template('mag-half-width-text-slider.php') ||
			is_page_template('mag-half-width-thumbnails-slider.php')) &&
			(of_get_option('slide_image_1') ||
			of_get_option('slide_image_2') ||
			of_get_option('slide_image_3') ||
			of_get_option('slide_image_4') ||
			of_get_option('slide_image_5'))
		):
		?>
		jQuery('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails",
			start: function() {
				jQuery('.flex-control-thumbs img').wrap('<strong><span></span></strong>');
				jQuery('.flex-control-thumbs img.active').parents('li').addClass('active');
				//jQuery('.flex-direction-nav').appendTo('.slides');
			},
			before: function() {
				jQuery('.flex-control-thumbs img.active').parents('li').removeClass('active');
				jQuery('.flex-direction-nav').hide();
			},
			after: function() {
				jQuery('.flex-control-thumbs img.active').parents('li').addClass('active');
				jQuery('.flex-direction-nav').show();
			}
		});
		<?php endif; ?>

		jQuery('.jcarousel-skin-media').jcarousel({
			scroll: 1,
			visible: 3,
			buttonNextHTML: '<a href="#" class="next">next</a>',
			buttonPrevHTML: '<a href="#" class="prev">prev</a>',
		});

		jQuery('.jcarousel-skin-media .prev, .jcarousel-skin-media .next').live('click', function(e) {
			e.preventDefault();
		});
	});
	</script>
</head>
	<body <?php body_class(); ?>>
		<div id="wrapper">
			<div id="header" class="group">
				<div class="holder logo-holder">
					<div class="logo-box">
						<a href="<?php bloginfo('wpurl'); ?>"><img src="<?php echo of_get_option('logo', get_bloginfo('template_directory')."/images/logo-aotopo.png"); ?>" alt="<?php bloginfo('name'); ?>" /></a>
					</div>
					<?php if(of_get_option('header_ad')): ?>
					<div class="ads">
						<?php echo of_get_option('header_ad'); ?>
					</div>
					<?php endif; ?>
				<div class="top-section">
					<div class="holder">
						<span class="btn-topo"><a href="#" title="Visitez Le Web au Brésil">Visitez Le Web au Brésil</a></span>
					</div>


					<!-- social BUTTON header -->
					<div id="social-header">
						
						<!-- GPLUS BUTTON -->
	                    <div id="g-plus-header">
							<div class="g-plusone" data-size="medium" data-href="https://plus.google.com/116136392359275426287"></div>
						</div>

						<!-- LINKEDIN BUTTON -->
						<div id="linkedin-header">
							<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
							<script type="IN/Share" data-counter="right"></script>
	                    </div>

					</div>


				</div>

				</div>
				<div class="nav-holder">
					<?php wp_nav_menu(array('theme_location' => 'main_navigation', 'depth' => 3, 'container' => false, 'menu_id' => 'nav')); ?>
				</div>
			</div>
			<div id="main">
                