<?php
/*
Template Name: servicos
*/
?>

<?php get_header(); ?>
<div class="holder">
	<div class="frame">
		<div class="main-content page-servicos">

			<?php while(have_posts()): the_post(); ?>

			<div id="content" style="width:100%;">
				
				<div class="heading">
					<?php if (function_exists('yoast_breadcrumb')){yoast_breadcrumb('<ul class="breadcrumbs">','</ul>');} ?>
					<br>
				</div>

				<h3 class="fixPageTitle"><?php the_title(); ?></h3>
				
				<div class="post" style="padding:0;">	

					<div class="post-content servicos" style="background:none; min-height:400px">
						
						<?php the_content(); ?>

						<div id="servicosHome-box">

							<div id="servicosHome-content">

								<ul id="servicosHome">

									<li>
										<a href="http://www.aotopo.com.br/servicos/web-design/">
											<figure class="servicos-home-imagem">

												<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-webdesign-over.png"?>" alt="Web Design" class="servicos-home-imagem-over">

												<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-webdesign.png"?>" alt="Web Design" class="servicos-home-imagem-out">

											</figure>
											<br>
											<div class="servicos-home-title">Web Design</div>
										</a>
									</li>

									<li>
										<a href="http://www.aotopo.com.br/servicos/inbound-marketing/">
											<figure class="servicos-home-imagem">

												<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-inbound-over.png"?>" alt="Web Design" class="servicos-home-imagem-over">

												<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-inbound.png"?>" alt="Web Design" class="servicos-home-imagem-out">

											</figure>
											<br>
											<div class="servicos-home-title">Inbound Marketing</div>
										</a>
									</li>

									<li>
										<a href="http://www.aotopo.com.br/servicos/parcerias/">
											<figure class="servicos-home-imagem">

												<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-parcerias-over.png"?>" alt="Web Design" class="servicos-home-imagem-over">

												<img src="<?php echo get_bloginfo('template_directory')."/images/servicos-home-parcerias.png"?>" alt="Web Design" class="servicos-home-imagem-out">

											</figure>
											<br>
											<div class="servicos-home-title">Parcerias</div>
										</a>
									</li>

								</ul>

							</div>
						</div>

					</div>
				</div>

			<?php endwhile; ?>

			<?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc'); ?>

		</div>
	</div>
</div>
<script src="<?php echo get_bloginfo('template_directory')."/js/jquery-1.8.2.js"?>"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory')."/js/servicos.js"?>"></script>
<?php get_footer(); ?>